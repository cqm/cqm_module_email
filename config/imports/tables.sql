-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 30, 2018 at 12:41 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


-- --------------------------------------------------------

--
-- Table structure for table `EML_aplicacion`
--

CREATE TABLE `EML_aplicacion` (
  `id` int(10) UNSIGNED NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_servidor_trans` int(11) NOT NULL,
  `id_servidor_mas` int(11) NOT NULL,
  `guardar_emails` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SYSTEM',
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `EML_contacto`
--

CREATE TABLE `EML_contacto` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idPlantilla` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `params` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SYSTEM',
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `EML_idioma_plantilla`
--

CREATE TABLE `EML_idioma_plantilla` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_plantilla` int(11) NOT NULL,
  `idioma` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `asunto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contenido` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SYSTEM',
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `EML_parametros`
--

CREATE TABLE `EML_parametros` (
  `id` int(10) UNSIGNED NOT NULL,
  `clave` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SYSTEM',
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `EML_plantilla`
--

CREATE TABLE `EML_plantilla` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_aplicacion` int(11) NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SYSTEM',
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `EML_robinson`
--

CREATE TABLE `EML_robinson` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_aplicacion` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SYSTEM',
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `EML_servidor`
--

CREATE TABLE `EML_servidor` (
  `id` int(10) UNSIGNED NOT NULL,
  `carrier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `puerto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dominio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `charset` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'utf8',
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `orden` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dir_remitente` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_remitente` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `responder_a` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `datos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contrasenia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SYSTEM',
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `EML_white_list`
--

CREATE TABLE `EML_white_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_aplicacion` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SYSTEM',
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `EML_aplicacion`
--
ALTER TABLE `EML_aplicacion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eml_aplicacion_id_unique` (`id`);

--
-- Indexes for table `EML_contacto`
--
ALTER TABLE `EML_contacto`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eml_contacto_id_unique` (`id`);

--
-- Indexes for table `EML_idioma_plantilla`
--
ALTER TABLE `EML_idioma_plantilla`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eml_idioma_plantilla_id_unique` (`id`);

--
-- Indexes for table `EML_parametros`
--
ALTER TABLE `EML_parametros`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eml_parametros_id_unique` (`id`),
  ADD UNIQUE KEY `eml_parametros_clave_unique` (`clave`);

--
-- Indexes for table `EML_plantilla`
--
ALTER TABLE `EML_plantilla`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eml_plantilla_id_unique` (`id`);

--
-- Indexes for table `EML_robinson`
--
ALTER TABLE `EML_robinson`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eml_robinson_id_unique` (`id`);

--
-- Indexes for table `EML_servidor`
--
ALTER TABLE `EML_servidor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eml_servidor_id_unique` (`id`);

--
-- Indexes for table `EML_white_list`
--
ALTER TABLE `EML_white_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eml_white_list_id_unique` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `EML_aplicacion`
--
ALTER TABLE `EML_aplicacion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `EML_contacto`
--
ALTER TABLE `EML_contacto`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `EML_idioma_plantilla`
--
ALTER TABLE `EML_idioma_plantilla`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `EML_parametros`
--
ALTER TABLE `EML_parametros`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `EML_plantilla`
--
ALTER TABLE `EML_plantilla`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `EML_robinson`
--
ALTER TABLE `EML_robinson`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `EML_servidor`
--
ALTER TABLE `EML_servidor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `EML_white_list`
--
ALTER TABLE `EML_white_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
