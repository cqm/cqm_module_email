<?php

namespace CQM\Modules\EML\Config;

class DBConfig
{
    const DB_DRIVER = 'pdo_mysql';
    const DB_USER = 'root';
    const DB_PASSWORD = '';
    const DB_HOST = 'localhost';
    const DB_PORT = 3306;
    const DB_DATABASE_NAME = '';
    const DB_CHARSET = 'utf8';
    const DB_COLLATION = 'utf8_unicode_ci';
    const DB_DEV_MODE = false;

    /* Application */
    const APP_ID = 1; // EML Backend
}
