<?php
// bootstrap.php
require_once(__DIR__."/../vendor/autoload.php");
require_once "db-config.php";

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\YamlDriver;
use Doctrine\ORM\Configuration;
use CQM\Modules\EML\Config\DBConfig;

$paths = array(__DIR__."/../src/Modules/EML/Resources/config/doctrine");
$isDevMode = true;
$config = Setup::createYAMLMetadataConfiguration($paths, $isDevMode);

// the connection configuration
$connectionParams = array(
    'driver'   => DBConfig::DB_DRIVER,
    'user'     => DBConfig::DB_USER,
    'password' => DBConfig::DB_PASSWORD,
    'host'     => DBConfig::DB_HOST,
    'dbname'   => DBConfig::DB_DATABASE_NAME,
);

$entityManager = EntityManager::create($connectionParams, $config);