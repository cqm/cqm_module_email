# CQM Email Module
## Módulo para enviar emails

### Installation

Añadir en `composer.json`:

```javascript
"require": {
    // ...
    "cqm/module-email": "^2.0"
},
"repositories": [
    {
        "type": "composer",
        "url": "https://packages.loyalia.net/",
        "name" : "https://packages.loyalia.net/"
    }
],
"scripts": {
    "cqm-module-install": [
        "CQM\\Modules\\EML\\Composer\\ModuleInstaller::install"
    ],
    "post-install-cmd": [
        "@cqm-module-install"
    ],
    "post-update-cmd": [
        "@cqm-module-install"
    ]
}
```

Composer pedirá los parámetros de conexión para instalar el módulo y actualizar el schema de la base de datos.  

### Usage

#### Inicializar el módulo

```php
<?php

use CQM\Modules\EML\EML;

EML::init($driver, $user, $pass, $host, $port, $database);
```

#### Enviar un mail a un destinatario

```php
<?php

use CQM\Modules\EML\EML;

$traveler = '';
$applicationId = 1; // Use your own application ID
// Recipient data
$mailTo = 'johndoe@example.com';
$firstName = 'John';
$lastName = 'Doe';
// Add your own key => value parameters to replace them in the contents of your mail
$parameters = array('link' => 'http://bitbucket.org');
// Prepare mail recipient
$recipient = EML::prepareRecipient($traveler, $mailTo, $firstName, $lastName, $parameters);

// Mail data
$subject = 'Check this out, {{firstName}}!';
$body = 'Check this repository on <a href="{{link}}">Bitbucket</a>';
// Create a Mail object
$mail = EML::createMail($traveler, $applicationId, $recipient, $subject, $body);
// Send it
EML::send($traveler, $mail);
```

#### Enviar un mail a un destinatario (a partir de una plantilla)

```php
<?php

use CQM\Modules\EML\EML;

$mailTo = 'johndoe@example.com';

// ...

// Prepare mail recipient
$recipient = EML::prepareRecipient($traveler, $mailTo, $firstName, $lastName, $parameters);

// Template data
$subject = 'My custom template';
$body = 'Check this repository on <a href="{{link}}">Bitbucket</a>';
// Create a template
$templateId = EML::createTemplate($traveler, $applicationId, $subject, $body);
// Create a Mail object
$mail = EML::createMailFromTemplate($traveler, $templateId, $recipient);
// Send it
EML::send($traveler, $mail, true);
```

#### Enviar un mail a varios destinatarios
```php
<?php

use CQM\Modules\EML\EML;

// store each recipient in an array
$recipients = array(
    EML::prepareRecipient($traveler, 'johndoe@example.com', 'John', 'Doe', array('link' => 'http://google.com')),
    EML::prepareRecipient($traveler, 'admin@example.com', 'Admin', 'Example', array('link' => 'http://github.com'))
);

// ...

// Create mail queue
$mailQueue = EML::createMailQueue($traveler, $applicationId, $recipients, null, $subject, $body);
// Send
EML::sendMailQueue($traveler, $mailQueue);
```

#### Enviar un mail a varios destinatarios (a partir de una plantilla)
```php
<?php

use CQM\Modules\EML\EML;

// store each recipient in an array
$recipients = array(
    EML::prepareRecipient($traveler, 'johndoe@example.com', 'John', 'Doe', array('link' => 'http://google.com')),
    EML::prepareRecipient($traveler, 'admin@example.com', 'Admin', 'Example', array('link' => 'http://github.com'))
);

// ...

// Create a template
$templateId = EML::createTemplate($traveler, $applicationId, $subject, $body);
// Create mail queue
$mailQueue = EML::createMailQueue($traveler, $applicationId, $recipients, $templateId);
// Send the mails in queue
EML::sendMailQueue($traveler, $mailQueue, true);
```



🛠️**Work In Progress**🛠️ (20/03/2019)

* ✅ Añadir reemplazos de textos en el body de los correos  
* ✅ Todos los métodos de la clase "pública" deben ser estáticos   
* ✅ Mover todos los métodos CRUD de las entidades a sus correspondientes repositorios  
* ✅ Tests funcionales con PHPUnit  