<?php

use \PHPUnit\Framework\TestCase;

use CQM\Modules\EML\EML;
use CQM\Modules\EML\Util\UtilDatabase;
use CQM\Modules\EML\Util\UtilMessage;
use CQM\Modules\EML\Entity\Application;
use CQM\Modules\EML\Entity\Server;
use CQM\Modules\EML\Entity\Mail;
use CQM\Modules\EML\Entity\Template;

class SMTPTest extends TestCase
{   
    // Application properties
    private $em = null;

    private $eml = null;

    private $applicationRepository;

    private $serverRepository;

    private $traveler = '';
    
    private $clientId = 0;

    private $description = 'Created from SMTPTest';

    private $transactionalServer = 1;
    
    private $bulkServer = 1;

    private $saveEmails = 0;

    private $application;

    private $serverDemo;

    private $mailTo = '';

    private $firstName = '';

    private $lastName = '';

    private $parameters = array('gender' => 'o');

    // Mail properties
    private $subject = 'Mail from SMTP Server (Office365) & PHPUnit';

    private $body = '<html><body>
                        <h2>Hello World!</h2>
                        <p>This is the first mail sent with our SMTP Server</p>
                    </body></html>';

    private $campaignName = 'Campaign test';

    private $language = 'ES';

    private $method = 'G';

    private $recipient = array();

    protected function setUp() : void
    {
        // Connection parameters
        $driver = 'pdo_mysql';
        $user = '';
        $password = '';
        $host = '';
        $port = 3306;
        $database = '';
        // Init EML class
        EML::init($driver, $user, $password, $host, $port, $database);
        // Get entity manager
        $this->em = UtilDatabase::getEntityManager();
        // Get Application Repository
        $this->applicationRepository = $this->em->getRepository(Application::class);
        // Get Server repository
        $this->serverRepository = $this->em->getRepository(Server::class);

        $carrier = 'SMTP';
        $ipAddress = '';
        $host = '';
        $port = 587;
        $name = '';
        $charset = 'utf8';
        $description = '';
        $senderId = 1;
        $senderMail = '';
        $senderName = '';
        $replyTo = '';
        $serverOrder = null;
        $serverData = '';
        $username = '';
        $password = '';

        $this->serverDemo = $this->serverRepository->createServer($this->traveler, $carrier, $ipAddress, $host, $port, $name, $charset, $description, $senderId, $senderMail, $senderName, $replyTo, $serverOrder, $serverData, $username, $password);

        $this->recipient = EML::prepareRecipient($this->traveler, $this->mailTo, $this->firstName, $this->lastName, $this->parameters);
    }

    /**
     * @test
     * @group Mail
     */
    public function canCreateMail()
    {
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);
        
        $mail = EML::createMail($this->traveler, $application->getId(), $this->recipient, $this->subject, $this->body);

        $traveler = json_decode($this->traveler, true);

        $this->assertEquals(Mail::class, get_class($mail));
        $this->assertStringContainsString(UtilMessage::getMessage(UtilMessage::MAIL_CREATED, 0), $traveler['output']['user_text']);
        $this->assertEquals(Application::class, get_class($mail->getApplication()));
        $this->assertEquals($mail->getApplication()->getId(), $application->getId());
    }

    /**
     * @test
     * @group Mail
     */
    public function canSendAndSaveMail()
    {
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);
        $method = 'G';
        $mail = EML::createMail($this->traveler, $application->getId(), $this->recipient, $this->subject, $this->body, 'ES', $method);
        
        EML::send($this->traveler, $mail);

        $traveler = json_decode($this->traveler, true);

        $this->assertEquals(1, $mail->getState());
        $this->assertStringContainsString(UtilMessage::getMessage(UtilMessage::MAIL_SEND_SUCCESS), $traveler['output']['user_text']);

        $this->em->remove($application);
        
    }   

    /**
     * Delete everything on cascade
     */
    protected function tearDown() : void
    {
        $applications = $this->applicationRepository->getApplications($this->traveler);

        foreach ($applications as $application) {
            $this->em->remove($application);
        }

        $this->em->remove($this->serverDemo);
        $this->em->flush();
    }
}  