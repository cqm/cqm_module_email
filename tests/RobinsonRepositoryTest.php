<?php

use CQM\Modules\EML\EML;
use \PHPUnit\Framework\TestCase;
use CQM\Modules\EML\Entity\Server;
use CQM\Modules\EML\Entity\Robinson;
use CQM\Modules\EML\Util\UtilMessage;
use CQM\Modules\EML\Util\UtilDatabase;
use CQM\Modules\EML\Entity\Application;

class RobinsonRepositoryTest extends TestCase
{   
    private $em = null;

    private $applicationRepository;

    private $serverRepository;

    private $robinsonRepository;

    private $eml = null;

    private $traveler = '';
    
    private $clientId = 0;

    private $description = 'Created from PHPUnit';

    private $transactionalServer = 1;
    
    private $bulkServer = 1;

    private $saveEmails = 0;

    private $application = null;

    private $serverDemo = null;

    protected function setUp() : void
    {
        // Connection parameters
        $driver = 'pdo_mysql';
        $user = '';
        $password = '';
        $host = '';
        $port = 3306;
        $database = '';
        // Init EML class
        EML::init($driver, $user, $password, $host, $port, $database);
        // Get entity manager
        $this->em = UtilDatabase::getEntityManager();
        // Get Server Repository
        $this->serverRepository = $this->em->getRepository(Server::class);
        // Get Application Repository
        $this->applicationRepository = $this->em->getRepository(Application::class);
        // Get Robinson Repository
        $this->robinsonRepository = $this->em->getRepository(Robinson::class);
        // Set up a server
        $carrier = '';
        $ipAddress = '';
        $host = '';
        $port = 0;
        $name = '';
        $charset = 'utf8';
        $description = '';
        $senderId = 1;
        $senderMail = '';
        $senderName = '';
        $replyTo = '';
        $serverOrder = null;
        $serverData = '';
        $username = '';
        $password = '';
        $plan = '';
        // Create one server to run tests
        $this->serverDemo = $this->serverRepository->createServer($this->traveler, $carrier, $ipAddress, $host, $port, $name, $charset, $description, $senderId, $senderMail, $senderName, $replyTo, $serverOrder, $serverData, $username, $password, $plan);
    }

    /** 
     * @test
     * @group RobinsonRepository
     */
    public function canAddEmailAddressToRobinson()
    {
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);

        $robinson = $this->robinsonRepository->addEmailAddressToRobinsonList($this->traveler, $application->getId(), 'akserikawa@gmail.com');

        $this->assertEquals('johndoe@example.com', $robinson->getEmail());
        $this->assertEquals(Robinson::class, get_class($robinson));
    }

    /** 
     * @test
     * @group RobinsonRepository
     */
    public function canGetEmailAddressFromRobinson()
    {
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);

        $robinson = $this->robinsonRepository->addEmailAddressToRobinsonList($this->traveler, $application->getId(), 'akserikawa@gmail.com');
        $robinsonList = $this->robinsonRepository->getEmailAddressesFromRobinsonList($this->traveler, $application->getId());

        $this->assertEquals(1, count($robinsonList));
        $this->assertContainsOnlyInstancesOf(Robinson::class, $robinsonList);
    }

    /** 
     * @test
     * @group RobinsonRepository
     */
    public function canRemoveEmailAddressFromRobinson()
    {
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);

        $robinson = $this->robinsonRepository->addEmailAddressToRobinsonList($this->traveler, $application->getId(), 'akserikawa@gmail.com');
        $removed = $this->robinsonRepository->removeEmailAddressFromRobinsonList($this->traveler, $application->getId(), 'akserikawa@gmail.com');

        $this->assertTrue($removed);
    }

    /**
     * Delete everything on cascade
     */
    protected function tearDown() : void
    {        
        $applications = $this->applicationRepository->getApplications();

        foreach ($applications as $application) {
            $this->em->remove($application);
        }
        
        $this->em->remove($this->serverDemo);
        $this->em->flush();
    }
}