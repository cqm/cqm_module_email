<?php

use \PHPUnit\Framework\TestCase;

use CQM\Modules\EML\EML;
use CQM\Modules\EML\Util\UtilDatabase;
use CQM\Modules\EML\Util\UtilMessage;
use CQM\Modules\EML\Entity\Application;
use CQM\Modules\EML\Entity\Server;
use CQM\Modules\EML\Entity\Template;

class ApplicationRepositoryTest extends TestCase
{   
    private $em = null;

    private $applicationRepository;

    private $serverRepository;

    private $eml = null;

    private $traveler = '';
    
    private $clientId = 0;

    private $description = 'Created from PHPUnit';

    private $transactionalServer = 1;
    
    private $bulkServer = 1;

    private $saveEmails = 0;

    private $application = null;

    private $serverDemo = null;

    protected function setUp() : void
    {
        // Connection parameters
        $driver = 'pdo_mysql';
        $user = '';
        $password = '';
        $host = 'localhost';
        $port = 3306;
        $database = '';
        // Init EML class
        EML::init($driver, $user, $password, $host, $port, $database);
        // Get entity manager
        $this->em = UtilDatabase::getEntityManager();
        // Get Application Repository
        $this->applicationRepository = $this->em->getRepository(Application::class);
        // Get Server Repository
        $this->serverRepository = $this->em->getRepository(Server::class);
        // Set up a server
        $carrier = '';
        $ipAddress = '';
        $host = '';
        $port = 0;
        $name = '';
        $charset = 'utf8';
        $description = '';
        $senderId = 1;
        $senderMail = '';
        $senderName = '';
        $replyTo = '';
        $serverOrder = null;
        $serverData = '';
        $username = '';
        $password = '';
        $plan = '';
        // Create one server to run tests
        $this->serverDemo = $this->serverRepository->createServer($this->traveler, $carrier, $ipAddress, $host, $port, $name, $charset, $description, $senderId, $senderMail, $senderName, $replyTo, $serverOrder, $serverData, $username, $password, $plan);
    }

    /** 
     * @test
     * @group ApplicationRepository
     */
    public function canCreateApplicationFromRepository()
    {   
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);
    
        $this->assertEquals(Application::class, get_class($application));
        $this->assertEquals($this->description, $application->getDescription());
    }

    /** 
     * @test
     * @group ApplicationRepository  
     */
    public function canGetApplicationFromRepository()
    {   
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);
        $application = $this->applicationRepository->getApplication($this->traveler, $application->getId());

        $this->assertEquals(Application::class, get_class($application));
    }

    /** 
     * @test
     * @group ApplicationRepository  
     */
    public function canUpdateApplicationFromRepository()
    {   
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);
        $applicationUpdated = $this->applicationRepository->updateApplication($this->traveler, $application->getId(), 'hola', $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);

        $this->assertEquals($application->getId(), $applicationUpdated->getId());
        $this->assertStringContainsString('hola', $applicationUpdated->getDescription());
    }

    /** 
     * @test
     * @group ApplicationRepository
     */
    public function canDeleteApplicationFromRepository()
    {
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);
        // store the ID before deleting it
        $id = $application->getId();
        $this->applicationRepository->deleteApplication($this->traveler, $id);
        $traveler = json_decode($this->traveler, true);

        $this->assertStringContainsString(UtilMessage::getMessage(UtilMessage::APPLICATION_DELETED, $id), $traveler['output']['user_text']);
    }

    /**
     * Delete everything on cascade
     */
    protected function tearDown() : void
    {        
        $applications = $this->applicationRepository->getApplications();

        foreach ($applications as $application) {
            $this->em->remove($application);
        }
        
        $this->em->remove($this->serverDemo);
        $this->em->flush();
    }
}
