<?php

use \PHPUnit\Framework\TestCase;

use CQM\Modules\EML\EML;
use CQM\Modules\EML\Util\UtilDatabase;
use CQM\Modules\EML\Util\UtilMessage;
use CQM\Modules\EML\Entity\Application;
use CQM\Modules\EML\Entity\Server;
use CQM\Modules\EML\Entity\Template;
use CQM\Modules\EML\Entity\Contact;

class TemplateRepositoryTest extends TestCase
{
    private $em = null;

    private $eml = null;

    private $traveler = '';
    
    private $clientId = 0;

    private $description = 'Created from PHPUnit';

    private $transactionalServer = 1;
    
    private $bulkServer = 1;

    private $saveEmails = 0;

    private $application = null;

    private $serverDemo = null;

    protected function setUp() : void
    {
        // Connection parameters
        $driver = 'pdo_mysql';
        $user = '';
        $password = '';
        $host = '';
        $port = 3306;
        $database = '';
        // Init EML class
        EML::init($driver, $user, $password, $host, $port, $database);
        
        // Get entity manager
        $this->em = UtilDatabase::getEntityManager();
        // Get Application Repository
        $this->applicationRepository = $this->em->getRepository(Application::class);
        // Get Server Repository
        $this->serverRepository = $this->em->getRepository(Server::class);
        // Get Template Repository
        $this->templateRepository = $this->em->getRepository(Template::class);
        // Find one server
        $carrier = '';
        $ipAddress = '';
        $host = '';
        $port = 0;
        $name = '';
        $charset = 'utf8';
        $description = 'Created from PHPUnit';
        $senderId = 1;
        $senderMail = '';
        $senderName = '';
        $replyTo = '';
        $serverOrder = null;
        $serverData = '';
        $username = '';
        $password = '';
        $plan = '';

        $this->serverDemo = $this->serverRepository->createServer($this->traveler, $carrier, $ipAddress, $host, $port, $name, $charset, $description, $senderId, $senderMail, $senderName, $replyTo, $serverOrder, $serverData, $username, $password, $plan);
    }   

    /** 
     * @test
     * @group TemplateRepository
     */
    public function canCreateTemplateFromRepository()
    {  
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);
        $templateId = EML::createTemplate($this->traveler, $application->getId(), 'Subject demo', 'Contents here');

        $template = $this->templateRepository->getTemplate($this->traveler, $templateId);

        $this->assertTrue($template instanceof Template);
        $this->assertEquals('Subject demo', $template->getContent()[0]->getSubject());
        $this->assertEquals('Contents here', $template->getContent()[0]->getBody());
    }

    /** 
     * @test
     * @group TemplateRepository
     */
    public function canGetTemplateFromRepository()
    {
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);
        $templateId = EML::createTemplate($this->traveler, $application->getId(), 'Subject demo', 'Contents here');

        $template = $this->templateRepository->getTemplate($this->traveler, $templateId);

        $this->assertEquals(Template::class, get_class($template));
        $this->assertContainsOnlyInstancesOf(Template::class, $application->getTemplate());
    }

    /** 
     * @test
     * @group TemplateRepository  
     */
    public function canUpdateTemplateContentsFromRepository()
    {  
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);
        $templateId = EML::createTemplate($this->traveler, $application->getId(), 'Subject demo', 'Template contents');
            
        $template = $this->templateRepository->getTemplate($this->traveler, $templateId);

        $templateId = EML::updateTemplate($this->traveler, $templateId, 'Updated subject', 'Updated contents');
        
        $templateUpdate = $this->templateRepository->getTemplate($this->traveler, $templateId);
        
        $this->assertSame($template->getId(), $templateUpdate->getId());
        $this->assertEquals('Updated subject', $templateUpdate->getContent()[0]->getSubject());
        $this->assertEquals('Updated contents', $templateUpdate->getContent()[0]->getBody());
    }

    /** 
     * @test
     * @group TemplateRepository  
     */
    public function canDeleteTemplateFromRepository()
    {
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);
        $templateId = EML::createTemplate($this->traveler, $application->getId(), 'Subject demo', 'Template contents');
        // Store the id
        $id = $templateId;
        // Delete the template
        $this->templateRepository->deleteTemplate($this->traveler, $id);

        $traveler = json_decode($this->traveler, true);
        // Try to find it, null return expected
        $template = $this->em->find(Template::class, $id);

        $this->assertNull($template);
        $this->assertStringContainsString(UtilMessage::getMessage(UtilMessage::TEMPLATE_DELETED, $id), $traveler['output']['user_text']);
    }

    /**     
     * @group TemplateRepository    
     */
    public function canAddContactsToTemplateFromRepository()
    {
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);
        $templateId = EML::createTemplate($this->traveler, $application->getId(), 'Subject demo', 'Template contents');

        $template = $this->templateRepository->getTemplate($this->traveler, $templateId);

        $contactList = [
            [
                'email' => 'aserikawa@chequemotiva.com',
                'firstName' => 'Akira',
                'lastName' => 'Serikawa'
            ]
        ];

        $template = $this->templateRepository->addContactsToTemplate($this->traveler, $template->getId(), $contactList);

        $traveler = json_decode($this->traveler, true);

        $this->assertContainsOnlyInstancesOf(Contact::class, $template->getContact());
        $this->assertEquals(1, count($template->getContact()));
        $this->assertStringContainsString(UtilMessage::getMessage(UtilMessage::TEMPLATE_CONTACTS_ADDED, $template->getId()), $traveler['output']['user_text']);

        $this->em->remove($application);
    }

    /**     
     * @group TemplateRepository    
     */
    public function canRemoveContactsFromTemplate()
    {
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);
        $templateId = EML::createTemplate($this->traveler, $application->getId(), 'Subject demo', 'Template contents');

        $template = $this->templateRepository->getTemplate($this->traveler, $templateId);

        $contactList = [
            [
                'email' => 'aserikawa@chequemotiva.com',
                'firstName' => 'Akira',
                'lastName' => 'Serikawa'
            ]
        ];
        // Add contacts and remove the ones from the list
        $this->templateRepository->addContactsToTemplate($this->traveler, $template->getId(), $contactList);
        $this->templateRepository->removeContactsFromTemplate($this->traveler, $template->getId(), $contactList);

        $traveler = json_decode($this->traveler, true);

        $this->assertEmpty($template->getContact());
        $this->assertEquals(0, count($template->getContact()));
        $this->assertStringContainsString(UtilMessage::getMessage(UtilMessage::TEMPLATE_CONTACTS_REMOVED, $template->getId()), $traveler['output']['user_text']);

        // Add contacts and remove all of them
        $this->templateRepository->addContactsToTemplate($this->traveler, $template->getId(), $contactList);
        $this->templateRepository->removeContactsFromTemplate($this->traveler, $template->getId());

        $this->assertEmpty($template->getContact());
        $this->assertEquals(0, count($template->getContact()));

        $this->em->remove($application);
    }

    /**
     * Delete everything on cascade
     */
    protected function tearDown() : void
    {
        $applications = $this->applicationRepository->getApplications($this->traveler);

        foreach ($applications as $application) {
            $this->em->remove($application);
        }

        $this->em->remove($this->serverDemo);
        $this->em->flush();
    }
}