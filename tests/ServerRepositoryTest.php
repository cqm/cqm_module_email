<?php

use \PHPUnit\Framework\TestCase;

use CQM\Modules\EML\EML;
use CQM\Modules\EML\Util\UtilDatabase;
use CQM\Modules\EML\Util\UtilMessage;
use CQM\Modules\EML\Entity\Application;
use CQM\Modules\EML\Entity\Server;
use CQM\Modules\EML\Entity\Template;

class ServerRepositoryTest extends TestCase
{  
    private $em = null;

    private $traveler = '';

    protected function setUp() : void
    {
        // Connection parameters
        $driver = 'pdo_mysql';
        $user = '';
        $password = '';
        $host = '';
        $port = 3306;
        $database = '';
        // Init EML class
        EML::init($driver, $user, $password, $host, $port, $database);
        // Get entity manager
        $this->em = UtilDatabase::getEntityManager();
        // Get Server Repository
        $this->serverRepository = $this->em->getRepository(Server::class);
    }

    /**
     * @test
     * @group ServerRepository
     */
    public function canCreateSmtpServerFromRepository()
    {   
        $carrier = '';
        $ipAddress = '';
        $host = '';
        $port = 587;
        $name = '';
        $charset = 'utf8';
        $description = '';
        $senderId = 1;
        $senderMail = '';
        $senderName = '';
        $replyTo = '';
        $serverOrder = null;
        $serverData = '';
        $username = '';
        $password = '';

        $server = $this->serverRepository->createServer($this->traveler, $carrier, $ipAddress, $host, $port, $name, $charset, $description, $senderId, $senderMail, $senderName, $replyTo, $serverOrder, $serverData, $username, $password);
    
        $traveler = json_decode($this->traveler, true);
        
        $this->assertEquals(Server::class, get_class($server));
        $this->assertStringContainsString(UtilMessage::getMessage(UtilMessage::SERVER_CREATED, $server->getId()), $traveler['output']['user_text']); 
    }

    /**
     * @test
     * @group ServerRepository 
     */
    public function canCreateTeenvioServerFromRepository()
    {
        $carrier = '';
        $ipAddress = '';
        $host = '';
        $port = 0;
        $name = '';
        $charset = 'utf8';
        $description = '';
        $senderId = 1;
        $senderMail = '';
        $senderName = '';
        $replyTo = '';
        $serverOrder = null;
        $serverData = '';
        $username = '';
        $password = '';
        $plan = '';

        $server = $this->serverRepository->createServer($this->traveler, $carrier, $ipAddress, $host, $port, $name, $charset, $description, $senderId, $senderMail, $senderName, $replyTo, $serverOrder, $serverData, $username, $password, $plan);

        $traveler = json_decode($this->traveler, true);
        
        $this->assertEquals(Server::class, get_class($server));
        $this->assertStringContainsString(UtilMessage::getMessage(UtilMessage::SERVER_CREATED, $server->getId()), $traveler['output']['user_text']); 
    }

    /**
     * @test
     * @group ServerRepository
     */
    public function canGetServerFromRepository()
    {
        $carrier = '';
        $ipAddress = '';
        $host = '';
        $port = 587;
        $name = '';
        $charset = 'utf8';
        $description = '';
        $senderId = 1;
        $senderMail = '';
        $senderName = '';
        $replyTo = '';
        $serverOrder = null;
        $serverData = '';
        $username = '';
        $password = '';

        $server = $this->serverRepository->createServer($this->traveler, $carrier, $ipAddress, $host, $port, $name, $charset, $description, $senderId, $senderMail, $senderName, $replyTo, $serverOrder, $serverData, $username, $password);
        $server = $this->serverRepository->getServer($this->traveler, $server->getId());

        $this->assertEquals(Server::class, get_class($server));
    }

    /**
     * @test
     * @group ServerRepository
     */
    public function canDeleteServerFromRepository()
    {
        $carrier = '';
        $ipAddress = '';
        $host = '';
        $port = 587;
        $name = '';
        $charset = 'utf8';
        $description = '';
        $senderId = 1;
        $senderMail = '';
        $senderName = '';
        $replyTo = '';
        $serverOrder = null;
        $serverData = '';
        $username = '';
        $password = '';

        $server = $this->serverRepository->createServer($this->traveler, $carrier, $ipAddress, $host, $port, $name, $charset, $description, $senderId, $senderMail, $senderName, $replyTo, $serverOrder, $serverData, $username, $password);

        $serverId = $server->getId();
        $deleted = $this->serverRepository->deleteServer($this->traveler, $serverId);

        $traveler = json_decode($this->traveler, true);

        $this->assertTrue($deleted);
        $this->assertStringContainsString(UtilMessage::getMessage(UtilMessage::SERVER_DELETED, $serverId), $traveler['output']['user_text']); 
    }

    /**
     * Delete everything on cascade
     */
    protected function tearDown() : void
    {
        $servers = $this->serverRepository->getServers();

        foreach ($servers as $server) {
            $this->em->remove($server);
        }

        $this->em->flush();
    }
}