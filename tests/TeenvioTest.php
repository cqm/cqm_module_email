<?php

use \PHPUnit\Framework\TestCase;

use CQM\Modules\EML\EML;
use CQM\Modules\EML\Util\UtilDatabase;
use CQM\Modules\EML\Util\UtilMessage;
use CQM\Modules\EML\Entity\Application;
use CQM\Modules\EML\Entity\Server;
use CQM\Modules\EML\Entity\Mail;
use CQM\Modules\EML\Entity\Template;

class TeenvioTest extends TestCase
{   
    // Application properties
    private $em = null;

    private $eml = null;

    private $applicationRepository;

    private $serverRepository;

    private $templateRepository;

    private $traveler = '';
    
    private $clientId = 0;

    private $description = 'Created from SMTPTest';

    private $transactionalServer = 1;
    
    private $bulkServer = 1;

    private $saveEmails = 0;

    private $serverDemo;

    private $mailTo = '';

    private $firstName = '';

    private $lastName = '';

    private $parameters = array('gender' => 'o');

    // Mail properties
    private $subject = 'Mail sent using Teenvio POST API & PHPUnit';

    private $body = "<html><body>
                        <h2>Hello World!</h2>
                        <p>If you're seeing this it's too late</p>
                    </body></html>";

    private $campaignName = 'Campaign test';

    private $language = 'ES';

    private $method = 'G';

    private $recipient = array();

    protected function setUp() : void
    {
        // Connection parameters
        $driver = 'pdo_mysql';
        $user = '';
        $password = '';
        $host = '';
        $port = 3306;
        $database = '';
        // Init EML class
        EML::init($driver, $user, $password, $host, $port, $database);
        // Get entity manager
        $this->em = UtilDatabase::getEntityManager();
        // Get Application Repository
        $this->applicationRepository = $this->em->getRepository(Application::class);
        // Get Server repository
        $this->serverRepository = $this->em->getRepository(Server::class);
        // Get Template repository
        $this->templateRepository = $this->em->getRepository(Template::class);

        $carrier = '';
        $ipAddress = '';
        $host = '';
        $port = 0;
        $name = '';
        $charset = 'utf8';
        $description = 'Created from PHPUnit';
        $senderId = 1;
        $senderMail = '';
        $senderName = '';
        $replyTo = '';
        $serverOrder = null;
        $serverData = '';
        $username = '';
        $password = '';
        $plan = '';

        $this->serverDemo = $this->serverRepository->createServer($this->traveler, $carrier, $ipAddress, $host, $port, $name, $charset, $description, $senderId, $senderMail, $senderName, $replyTo, $serverOrder, $serverData, $username, $password, $plan);

        $this->recipient = EML::prepareRecipient($this->traveler, $this->mailTo, $this->firstName, $this->lastName, $this->parameters);
    }

    /**
     * @test
     * @group Teenvio
     */
    public function canSendAndSaveMail()
    {
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);
        $method = 'G';
        $mail = EML::createMail($this->traveler, $application->getId(), $this->recipient, $this->subject, $this->body, 'ES', $method);
        
        EML::send($this->traveler, $mail);

        $traveler = json_decode($this->traveler, true);

        $this->assertEquals(1, $mail->getState());
        $this->assertStringContainsString(UtilMessage::getMessage(UtilMessage::MAIL_SEND_SUCCESS), $traveler['output']['user_text']);

        $this->em->remove($application);   
    }

    /**
     * @test
     * @group Teenvio
     */
    public function canSendAndSaveMailCreatedFromTemplate()
    {
        $application = $this->applicationRepository->createApplication($this->traveler, $this->description, $this->serverDemo->getId(), $this->serverDemo->getId(), $this->saveEmails);
        // create template
        $templateId = EML::createTemplate($this->traveler, $application->getId(), $this->subject, $this->body);        
        // create mail
        $mail = EML::createMailFromTemplate($this->traveler, $templateId, $this->recipient);
        // send it using the template contents
        EML::send($this->traveler, $mail, true);

        $traveler = json_decode($this->traveler, true);

        $this->assertEquals(1, $mail->getState());
        $this->assertStringContainsString(UtilMessage::getMessage(UtilMessage::MAIL_SEND_SUCCESS), $traveler['output']['user_text']);

        $this->em->remove($application);
    }

    /**
     * Delete everything on cascade
     */
    protected function tearDown() : void
    {
        $applications = $this->applicationRepository->getApplications($this->traveler);

        foreach ($applications as $application) {
            $this->em->remove($application);
        }

        $this->em->remove($this->serverDemo);
        $this->em->flush();
    }
}