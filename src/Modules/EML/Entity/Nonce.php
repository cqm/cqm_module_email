<?php

namespace CQM\Modules\EML\Entity;

use Doctrine\Common\Collections\Criteria;

/**
 * Nonce
 */
class Nonce
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nonceKey;

    /**
     * @var string
     */
    private $createdBy = 'SYSTEM';

    /**
     * @var string
     */
    private $updatedBy = 'SYSTEM';

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \CQM\Modules\EML\Entity\Application
     */
    private $application;

    public function __construct($nonceStr)
    {  
        $this->setNonceKey($nonceStr); 
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nonceKey.
     *
     * @param string $nonceKey
     *
     * @return Nonce
     */
    public function setNonceKey($nonceKey)
    {
        if (!empty($nonceKey)) {
            $this->nonceKey = $nonceKey;
        }

        return $this;
    }

    /**
     * Get nonceKey.
     *
     * @return string
     */
    public function getNonceKey()
    {
        return $this->nonceKey;
    }

    /**
     * Set createdBy.
     *
     * @param string $createdBy
     *
     * @return Nonce
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param string $updatedBy
     *
     * @return Nonce
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Nonce
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Nonce
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set application.
     *
     * @param \CQM\Modules\EML\Entity\Application|null $application
     *
     * @return Nonce
     */
    public function setApplication(\CQM\Modules\EML\Entity\Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application.
     *
     * @return \CQM\Modules\EML\Entity\Application|null
     */
    public function getApplication()
    {
        return $this->application;
    }
    
    /**
     * Set timestamps
     */
    public function updatedTimestamps()
    {
        $dateTimeNow = new \DateTime('now');
        $this->setUpdatedAt($dateTimeNow);
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }
}
