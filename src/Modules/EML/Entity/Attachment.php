<?php

namespace CQM\Modules\EML\Entity;

use Doctrine\Common\Collections\Criteria;

/**
 * Attachment
 */
class Attachment
{      
    private $id;

    /**
     * Base 64 encoded file content
     *
     * @var string
     */
    private $data;

    private $originalFileName;
    
    /**
     * Original file size in bytes
     *
     * @var string
     */
    private $originalFileSize;

    /**
     * @var string
     */
    private $mimeType;

    /**
     * @var string
     */
    private $fileExtension;
    
    /**
     * @var string
     */
    private $tempPath;

    /**
     * @var string
     */
    private $createdBy = 'SYSTEM';

    /**
     * @var string
     */
    private $updatedBy = 'SYSTEM';

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \CQM\Modules\EML\Entity\Mail
     */
    private $mail;


    public function getId()
    {
        return $this->id;
    }

    public function setData($data)
    {
        $this->data = \base64_encode($data);

        return $this;
    }

    public function getData()
    {
        return \base64_decode($this->data);
    }

    public function getEncodedData()
    {
        return $this->data;
    }

    /**
     * Set mail.
     *
     * @param \CQM\Modules\EML\Entity\Mail|null $mail
     *
     * @return Mail
     */
    public function setMail(\CQM\Modules\EML\Entity\Mail $mail = null)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail.
     *
     * @return \CQM\Modules\EML\Entity\Mail|null
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set createdBy.
     *
     * @param string $createdBy
     *
     * @return Mail
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param string $updatedBy
     *
     * @return Mail
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Mail
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Mail
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set timestamps
     */
    public function updatedTimestamps()
    {
        $dateTimeNow = new \DateTime('now');
        $this->setUpdatedAt($dateTimeNow);
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }

    /**
     * @param string $originalFileName 
     * 
     * @return self
     */
    public function setOriginalFileName($originalFileName)
    {
        $this->originalFileName = $originalFileName;

        return $this;
    }

    public function getOriginalFileName()
    {
        return $this->originalFileName;
    }

    /**
     * @param string $originalFileSize Original file size in bytes
     * 
     * @return self
     */
    public function setOriginalFileSize(string $originalFileSize)
    {
        $this->originalFileSize = $originalFileSize;

        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalFileSize()
    {
        return $this->originalFileSize;
    }

    /**
     * @param string $mimeType 
     * 
     * @return self
     */
    public function setMimeType(string $mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @param string $fileExtension 
     * 
     * @return self
     */
    public function setFileExtension(string $fileExtension)
    {
        $this->fileExtension = $fileExtension;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileExtension()
    {
        return $this->fileExtension;
    }


    /**
     * @return string
     */
    public function getTempPath()
    {
        return $this->tempPath;
    }

    /**
     * @param string $tempPath 
     * 
     * @return self
     */
    public function setTempPath(string $tempPath)
    {
        $this->tempPath = $tempPath;

        return $this;
    }
}