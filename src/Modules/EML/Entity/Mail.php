<?php

namespace CQM\Modules\EML\Entity;

/**
 * Mail
 */
class Mail
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $mailTo;

    /**
     * @var string|null
     */
    private $mailToName;

    /**
     * @var string|null
     */
    private $fromMail;

    /**
     * @var string|null
     */
    private $fromName;

    /**
     * @var string|null
     */
    private $cc;

    /**
     * @var string|null
     */
    private $bcc;

    /**
     * @var string
     */
    private $priority = '1';

    /**
     * @var string|null
     */
    private $replyTo;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string|null
     */
    private $controlData;

    /**
     * @var string|null
     */
    private $sendId;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $body;

    /**
     * @var string
     */
    private $language;

    /**
     * @var array|null
     */
    private $replaceParameters;

    /**
     * @var array|null
     */
    private $filesAttached;

    /**
     * @var string
     */
    private $method = 'N';

    /**
     * @var string|null
     */
    private $senderId;

    /**
     * @var string|null
     */
    private $campaignName;

    /**
     * @var string|null
     */
    private $carrier;

    /**
     * @var string|null
     */
    private $groupId;

    /**
     * @var \DateTime|null
     */
    private $dateSend;

    /**
     * @var \DateTime|null
     */
    private $dateSent;

    /**
     * @var string
     */
    private $createdBy = 'SYSTEM';

    /**
     * @var string
     */
    private $updatedBy = 'SYSTEM';

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \CQM\Modules\EML\Entity\Application
     */
    private $application;

    /**
     * @var \CQM\Modules\EML\Entity\Template
     */
    private $template;

    /**
     * @var \CQM\Modules\EML\Entity\Server
     */
    private $server;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $attachment;

    public function __construct()
    {
        $this->attachment = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mailTo.
     *
     * @param string $mailTo
     *
     * @return Mail
     */
    public function setMailTo($mailTo)
    {
        $this->mailTo = $mailTo;

        return $this;
    }

    /**
     * Get mailTo.
     *
     * @return string
     */
    public function getMailTo()
    {
        return $this->mailTo;
    }

    /**
     * Set mailToName.
     *
     * @param string|null $mailToName
     *
     * @return Mail
     */
    public function setMailToName($mailToName = null)
    {
        $this->mailToName = $mailToName;

        return $this;
    }

    /**
     * Get mailToName.
     *
     * @return string|null
     */
    public function getMailToName()
    {
        return $this->mailToName;
    }

    /**
     * Set fromMail.
     *
     * @param string|null $fromMail
     *
     * @return Mail
     */
    public function setFromMail($fromMail = null)
    {
        $this->fromMail = $fromMail;

        return $this;
    }

    /**
     * Get fromMail.
     *
     * @return string|null
     */
    public function getFromMail()
    {
        return $this->fromMail;
    }

    /**
     * Set fromName.
     *
     * @param string|null $fromName
     *
     * @return Mail
     */
    public function setFromName($fromName = null)
    {
        $this->fromName = $fromName;

        return $this;
    }

    /**
     * Get fromName.
     *
     * @return string|null
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * Set cc.
     *
     * @param string|null $cc
     *
     * @return Mail
     */
    public function setCc($cc = null)
    {
        $this->cc = $cc;

        return $this;
    }

    /**
     * Get cc.
     *
     * @return string|null
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * Set bcc.
     *
     * @param string|null $bcc
     *
     * @return Mail
     */
    public function setBcc($bcc = null)
    {
        $this->bcc = $bcc;

        return $this;
    }

    /**
     * Get bcc.
     *
     * @return string|null
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * Set priority.
     *
     * @param string $priority
     *
     * @return Mail
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority.
     *
     * @return string
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set replyTo.
     *
     * @param string|null $replyTo
     *
     * @return Mail
     */
    public function setReplyTo($replyTo = null)
    {
        $this->replyTo = $replyTo;

        return $this;
    }

    /**
     * Get replyTo.
     *
     * @return string|null
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * Set state.
     *
     * @param string $state
     *
     * @return Mail
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state.
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set controlData.
     *
     * @param string|null $controlData
     *
     * @return Mail
     */
    public function setControlData($controlData = null)
    {
        $this->controlData = $controlData;

        return $this;
    }

    /**
     * Get controlData.
     *
     * @return string|null
     */
    public function getControlData()
    {
        return $this->controlData;
    }

    /**
     * Set sendId.
     *
     * @param string|null $sendId
     *
     * @return Mail
     */
    public function setSendId($sendId = null)
    {
        $this->sendId = $sendId;

        return $this;
    }

    /**
     * Get sendId.
     *
     * @return string|null
     */
    public function getSendId()
    {
        return $this->sendId;
    }

    /**
     * Set subject.
     *
     * @param string $subject
     *
     * @return Mail
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set body.
     *
     * @param string $body
     *
     * @return Mail
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set language.
     *
     * @param string $language
     *
     * @return Mail
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language.
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set replaceParameters.
     *
     * @param array|null $replaceParameters
     *
     * @return Mail
     */
    public function setReplaceParameters($replaceParameters = null)
    {
        $this->replaceParameters = $replaceParameters;

        return $this;
    }

    /**
     * Get replaceParameters.
     *
     * @return array|null
     */
    public function getReplaceParameters()
    {
        return $this->replaceParameters;
    }

    /**
     * Set filesAttached.
     *
     * @param array|null $filesAttached
     *
     * @return Mail
     */
    public function setFilesAttached($filesAttached = null)
    {
        $this->filesAttached = $filesAttached;

        return $this;
    }

    /**
     * Get filesAttached.
     *
     * @return array|null
     */
    public function getFilesAttached()
    {
        return $this->filesAttached;
    }

    /**
     * Set method.
     *
     * @param string $method
     *
     * @return Mail
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method.
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set senderId.
     *
     * @param string|null $senderId
     *
     * @return Mail
     */
    public function setSenderId($senderId = null)
    {
        $this->senderId = $senderId;

        return $this;
    }

    /**
     * Get senderId.
     *
     * @return string|null
     */
    public function getSenderId()
    {
        return $this->senderId;
    }

    /**
     * Set campaignName.
     *
     * @param string|null $campaignName
     *
     * @return Mail
     */
    public function setCampaignName($campaignName = null)
    {
        $this->campaignName = $campaignName;

        return $this;
    }

    /**
     * Get campaignName.
     *
     * @return string|null
     */
    public function getCampaignName()
    {
        return $this->campaignName;
    }

    /**
     * Set carrier.
     *
     * @param string|null $carrier
     *
     * @return Mail
     */
    public function setCarrier($carrier = null)
    {
        $this->carrier = $carrier;

        return $this;
    }

    /**
     * Get carrier.
     *
     * @return string|null
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * Set groupId.
     *
     * @param string|null $groupId
     *
     * @return Mail
     */
    public function setGroupId($groupId = null)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Get groupId.
     *
     * @return string|null
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set dateSend.
     *
     * @param \DateTime|null $dateSend
     *
     * @return Mail
     */
    public function setDateSend($dateSend = null)
    {
        $this->dateSend = $dateSend;

        return $this;
    }

    /**
     * Get dateSend.
     *
     * @return \DateTime|null
     */
    public function getDateSend()
    {
        return $this->dateSend;
    }

    /**
     * Set dateSent.
     *
     * @param \DateTime|null $dateSent
     *
     * @return Mail
     */
    public function setDateSent($dateSent = null)
    {
        $this->dateSent = $dateSent;

        return $this;
    }

    /**
     * Get dateSent.
     *
     * @return \DateTime|null
     */
    public function getDateSent()
    {
        return $this->dateSent;
    }

    /**
     * Set createdBy.
     *
     * @param string $createdBy
     *
     * @return Mail
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param string $updatedBy
     *
     * @return Mail
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Mail
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Mail
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set application.
     *
     * @param \CQM\Modules\EML\Entity\Application|null $application
     *
     * @return Mail
     */
    public function setApplication(\CQM\Modules\EML\Entity\Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application.
     *
     * @return \CQM\Modules\EML\Entity\Application|null
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set template.
     *
     * @param \CQM\Modules\EML\Entity\Template|null $template
     *
     * @return Mail
     */
    public function setTemplate(\CQM\Modules\EML\Entity\Template $template = null)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template.
     *
     * @return \CQM\Modules\EML\Entity\Template|null
     */
    public function getTemplate()
    {
        return $this->template;
    }
    
    /**
     * Set server.
     *
     * @param \CQM\Modules\EML\Entity\Server|null $server
     *
     * @return Mail
     */
    public function setServer(\CQM\Modules\EML\Entity\Server $server = null)
    {
        $this->server = $server;

        return $this;
    }

    /**
     * Get server.
     *
     * @return \CQM\Modules\EML\Entity\Server|null
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * Add attachment.
     *
     * @param \CQM\Modules\EML\Entity\Attachment $attachment
     *
     * @return Application
     */
    public function addAttachment(\CQM\Modules\EML\Entity\Attachment $attachment)
    {
        $this->attachment[] = $attachment;

        return $this;
    }

    /**
     * Remove attachment.
     *
     * @param \CQM\Modules\EML\Entity\Attachment $attachment
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAttachment(\CQM\Modules\EML\Entity\Attachment $attachment)
    {
        return $this->attachment->removeElement($attachment);
    }

    /**
     * Get attachment.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * Set timestamps
     */
    public function updatedTimestamps()
    {
        $dateTimeNow = new \DateTime('now');
        $this->setUpdatedAt($dateTimeNow);
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }
}
