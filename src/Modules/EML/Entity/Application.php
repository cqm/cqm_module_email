<?php

namespace CQM\Modules\EML\Entity;

use Doctrine\Common\Collections\Criteria;

/**
 * Application
 */
class Application
{      
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $apiSecret;

    /**
     * @var int
     */
    private $clientId = 0;

    /**
     * @var int
     */
    private $saveEmails;

    /**
     * @var string
     */
    private $createdBy = 'SYSTEM';

    /**
     * @var string
     */
    private $updatedBy = 'SYSTEM';

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $mail;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $template;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $contact;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $robinson;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $whitelist;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $nonce;

    /**
     * @var \CQM\Modules\EML\Entity\Server
     */
    private $transactionalServer;

    /**
     * @var \CQM\Modules\EML\Entity\Server
     */
    private $bulkServer;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mail = new \Doctrine\Common\Collections\ArrayCollection();
        $this->template = new \Doctrine\Common\Collections\ArrayCollection();
        $this->contact = new \Doctrine\Common\Collections\ArrayCollection();
        $this->robinson = new \Doctrine\Common\Collections\ArrayCollection();
        $this->whitelist = new \Doctrine\Common\Collections\ArrayCollection();
        $this->nonce = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Application
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set apiKey.
     *
     * @param string $apiKey
     *
     * @return Application
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get apiKey.
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Set apiSecret.
     *
     * @param string $apiSecret
     *
     * @return Application
     */
    public function setApiSecret($apiSecret)
    {
        $this->apiSecret = $apiSecret;

        return $this;
    }

    /**
     * Get ApiSecret.
     *
     * @return string
     */
    public function getApiSecret()
    {
        return $this->apiSecret;
    }

    /**
     * Set clientId.
     *
     * @param int $clientId
     *
     * @return Application
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId.
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set saveEmails.
     *
     * @param int $saveEmails
     *
     * @return Application
     */
    public function setSaveEmails($saveEmails)
    {
        $this->saveEmails = $saveEmails;

        return $this;
    }

    /**
     * Get saveEmails.
     *
     * @return int
     */
    public function getSaveEmails()
    {
        return $this->saveEmails;
    }

    /**
     * Set createdBy.
     *
     * @param string $createdBy
     *
     * @return Application
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param string $updatedBy
     *
     * @return Application
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Application
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Application
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add mail.
     *
     * @param \CQM\Modules\EML\Entity\Mail $mail
     *
     * @return Application
     */
    public function addMail(\CQM\Modules\EML\Entity\Mail $mail)
    {
        $this->mail[] = $mail;

        return $this;
    }

    /**
     * Remove mail.
     *
     * @param \CQM\Modules\EML\Entity\Mail $mail
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMail(\CQM\Modules\EML\Entity\Mail $mail)
    {
        return $this->mail->removeElement($mail);
    }

    /**
     * Get mail.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Add template.
     *
     * @param \CQM\Modules\EML\Entity\Template $template
     *
     * @return Application
     */
    public function addTemplate(\CQM\Modules\EML\Entity\Template $template)
    {
        $this->template[] = $template;

        return $this;
    }

    /**
     * Remove template.
     *
     * @param \CQM\Modules\EML\Entity\Template $template
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTemplate(\CQM\Modules\EML\Entity\Template $template)
    {
        return $this->template->removeElement($template);
    }

    /**
     * Get template.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Add contact.
     *
     * @param \CQM\Modules\EML\Entity\Contact $contact
     *
     * @return Application
     */
    public function addContact(\CQM\Modules\EML\Entity\Contact $contact)
    {
        $this->contact[] = $contact;

        return $this;
    }

    /**
     * Remove contact.
     *
     * @param \CQM\Modules\EML\Entity\Contact $contact
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeContact(\CQM\Modules\EML\Entity\Contact $contact)
    {
        return $this->contact->removeElement($contact);
    }
    
    /**
     * Get contact.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Add robinson.
     *
     * @param \CQM\Modules\EML\Entity\Robinson $robinson
     *
     * @return Application
     */
    public function addRobinson(\CQM\Modules\EML\Entity\Robinson $robinson)
    {
        $this->robinson[] = $robinson;

        return $this;
    }

    /**
     * Remove robinson.
     *
     * @param \CQM\Modules\EML\Entity\Robinson $robinson
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRobinson(\CQM\Modules\EML\Entity\Robinson $robinson)
    {
        return $this->robinson->removeElement($robinson);
    }

    /**
     * Get robinson.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRobinson()
    {
        return $this->robinson;
    }

    /**
     * Add whitelist.
     *
     * @param \CQM\Modules\EML\Entity\Whitelist $whitelist
     *
     * @return Application
     */
    public function addWhitelist(\CQM\Modules\EML\Entity\Whitelist $whitelist)
    {
        $this->whitelist[] = $whitelist;

        return $this;
    }

    /**
     * Remove whitelist.
     *
     * @param \CQM\Modules\EML\Entity\Whitelist $whitelist
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeWhitelist(\CQM\Modules\EML\Entity\Whitelist $whitelist)
    {
        return $this->whitelist->removeElement($whitelist);
    }

    /**
     * Get whitelist.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWhitelist()
    {
        return $this->whitelist;
    }

    /**
     * Add nonce.
     *
     * @param \CQM\Modules\EML\Entity\Nonce $nonce
     *
     * @return Application
     */
    public function addNonce(\CQM\Modules\EML\Entity\Nonce $nonce)
    {
        $this->nonce[] = $nonce;

        return $this;
    }

    /**
     * Remove nonce.
     *
     * @param \CQM\Modules\EML\Entity\Nonce $nonce
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeNonce(\CQM\Modules\EML\Entity\Nonce $nonce)
    {
        return $this->nonce->removeElement($nonce);
    }

    /**
     * Get nonce.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNonce()
    {
        return $this->nonce;
    }

    /**
     * Set transactionalServer.
     *
     * @param \CQM\Modules\EML\Entity\Server|null $transactionalServer
     *
     * @return Application
     */
    public function setTransactionalServer(\CQM\Modules\EML\Entity\Server $transactionalServer = null)
    {
        $this->transactionalServer = $transactionalServer;

        return $this;
    }

    /**
     * Get transactionalServer.
     *
     * @return \CQM\Modules\EML\Entity\Server|null
     */
    public function getTransactionalServer()
    {
        return $this->transactionalServer;
    }

    /**
     * Set bulkServer.
     *
     * @param \CQM\Modules\EML\Entity\Server|null $bulkServer
     *
     * @return Application
     */
    public function setBulkServer(\CQM\Modules\EML\Entity\Server $bulkServer = null)
    {
        $this->bulkServer = $bulkServer;

        return $this;
    }

    /**
     * Get bulkServer.
     *
     * @return \CQM\Modules\EML\Entity\Server|null
     */
    public function getBulkServer()
    {
        return $this->bulkServer;
    }

    /**
     * Set timestamps
     */
    public function updatedTimestamps()
    {
        $dateTimeNow = new \DateTime('now');
        $this->setUpdatedAt($dateTimeNow);
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }

    public function checkRobinsonList($email)
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("email", $email))
            ->setMaxResults(1)
        ;

        return count($this->robinson->matching($criteria));
    }

    public function checkWhiteList($email)
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("email", $email))
            ->setMaxResults(1)
        ;

        return count($this->whitelist->matching($criteria));
    }

    public function hasContacts()
    {
        return count($this->contact);
    }

    /**
     * Return true if nonce is unique, false if it's repeated
     * @return bool 
     */
    public function checkNonceIsValid($nonceKey)
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("nonceKey", $nonceKey))
            ->setMaxResults(1)
        ;

        return (bool) !count($this->nonce->matching($criteria));
    }

    /**
     * Clean outdated nonces (older than the specified time)
     */
    public function cleanOutdatedNonces()
    {
        $dateTime = new \DateTime("now");
        $dateTime->modify("-5 minutes");

        $criteria = Criteria::create()->where(
            Criteria::expr()->lt("createdAt", $dateTime)
        );

        $nonces = $this->nonce->matching($criteria);

        if (count($nonces) > 0) {
            foreach ($nonces as $nonce) {
                $this->removeNonce($nonce);
            }
        }
    }
}
