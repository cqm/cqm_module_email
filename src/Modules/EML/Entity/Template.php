<?php

namespace CQM\Modules\EML\Entity;

/**
 * Template
 */
class Template
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $createdBy = 'SYSTEM';

    /**
     * @var string
     */
    private $updatedBy = 'SYSTEM';

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \CQM\Modules\EML\Entity\Application
     */
    private $application;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $content;

    public function __construct()
    {
        $this->content = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdBy.
     *
     * @param string $createdBy
     *
     * @return Template
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param string $updatedBy
     *
     * @return Template
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Template
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Template
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    /**
     * Set application.
     *
     * @param \CQM\Modules\EML\Entity\Application|null $application
     *
     * @return Template
     */
    public function setApplication(\CQM\Modules\EML\Entity\Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application.
     *
     * @return \CQM\Modules\EML\Entity\Application|null
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Add content.
     *
     * @param \CQM\Modules\EML\Entity\Content $content
     *
     * @return Template
     */
    public function addContent(\CQM\Modules\EML\Entity\Content $content)
    {
        $this->content[] = $content;

        return $this;
    }

    /**
     * Remove content.
     *
     * @param \CQM\Modules\EML\Entity\Content $content
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeContent(\CQM\Modules\EML\Entity\Content $content)
    {
        return $this->content->removeElement($content);
    }

    /**
     * Get content.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set timestamps
     */
    public function updatedTimestamps()
    {
        $dateTimeNow = new \DateTime('now');
        $this->setUpdatedAt($dateTimeNow);
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }


}
