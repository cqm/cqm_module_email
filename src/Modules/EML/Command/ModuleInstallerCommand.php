<?php

namespace CQM\Modules\EML\Command;

use CQM\Modules\EML\Composer\ScriptHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ModuleInstallerCommand extends Command
{
    const COMMAND_NAME = 'cqm:module-install';
    const COMMAND_DESCRIPTION = 'CQM Module installation. Asks for database connection parameters and updates the schema.';

    const TITLE = 'CQM Email Module installation';
    const FIRST_LINE = 'To complete the <info>CQM Email Module</> installation, an update of your database schema is required.';
    const SECOND_LINE = 'To do so, some connection parameters are needed (database host, user, password...)';
    const PROCEED_MESSAGE = 'Do you want to proceed with the installation?';
    
    const INSTRUCTION_MESSAGE = 'Provide the following parameters, press <Enter> to use the default values';

    const DB_DRIVER_DESC = 'Database driver';
    const DB_HOST_DESC = 'Database host';
    const DB_PORT_DESC = 'Database port';
    const DB_USER_DESC = 'Database user';
    const DB_PASS_DESC = 'Database password';
    const DB_NAME_DESC = 'Database name';
    const DB_DRIVER_DEFAULT = 'pdo_mysql';
    const DB_HOST_DEFAULT = '127.0.0.1';
    const DB_PORT_DEFAULT = 3306;
    const DB_USER_DEFAULT = 'root';
    
    const CONFIRMATION_MESSAGE = 'This will update your database schema. Continue?';

    const SCHEMA_UPDATE_LOADING = 'Updating schema...';
    const SCHEMA_UPDATE_SUCCESS = '<info>Schema updated!</>';

    const INSTALLATION_COMPLETE = '✅ CQM Email Module installed succesfully!';
    const INSTALLATION_INCOMPLETE = 'Installation incomplete. Run the command: ./bin/console cqm:module-install to complete the installation.';
    const INSTALLATION_FAILED = '! [ERROR] Installation failed. Some of the submitted parameters are not valid. Please, run the command: ./bin/console cqm:module-install to complete the installation.';

    const INVALID_PORT_MESSAGE = 'You must type a valid port number.';
    const INVALID_STRING_MESSAGE = 'You must provide a valid string.';

    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription(self::COMMAND_DESCRIPTION);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $io = new SymfonyStyle($input, $output);
        $io->title(self::TITLE);

        $output->writeln(self::FIRST_LINE);
        $output->writeln(self::SECOND_LINE);
        
        if (!$io->confirm(self::PROCEED_MESSAGE, true)) {
            $io->newLine();
            $io->note(self::INSTALLATION_INCOMPLETE);
            return;
        }
        
        $io->newLine();

        $io->text(self::INSTRUCTION_MESSAGE);

        $io->newLine();

        $params['db_driver'] = $io->ask(self::DB_DRIVER_DESC, self::DB_DRIVER_DEFAULT);
        $params['db_host'] = $io->ask(self::DB_HOST_DESC, self::DB_HOST_DEFAULT);
        $params['db_port'] = $io->ask(self::DB_PORT_DESC, self::DB_PORT_DEFAULT, function($number) { 
            return $this->validateInputNumber($number);
        });
        $params['db_user'] = $io->ask(self::DB_USER_DESC, self::DB_USER_DEFAULT);
        $params['db_pass'] = $io->askHidden(self::DB_PASS_DESC);
        $params['db_name'] = $io->ask(self::DB_NAME_DESC, null, function($string) {
            return $this->validateStringNotEmpty($string);
        });

        $io->newLine();

        if (!$io->confirm(self::CONFIRMATION_MESSAGE, true)) {
            $io->newLine();
            $io->note(self::INSTALLATION_INCOMPLETE);
            return;
        }

        $io->newLine();

        // start
        $io->progressStart(1);
        $io->text(self::SCHEMA_UPDATE_LOADING);
        $io->progressAdvance();
        // Send params to script handler and update the schema
        $response = ScriptHandler::updateSchema($params);
        $io->text(self::SCHEMA_UPDATE_SUCCESS);
        // end 
        
        $io->newLine();

        if ($response) {
            $output->writeln(self::INSTALLATION_COMPLETE);
        } else {
            $io->newLine();
            throw new \RuntimeException(self::INSTALLATION_FAILED);
        }
    }

    public function validateStringNotEmpty($string)
    {
        if ($string == '') {
            throw new \RuntimeException(self::INVALID_STRING_MESSAGE);
        }

        return $string;
    }
    
    public function validateInputNumber($number)
    {
        if (!is_numeric($number)) {
            throw new \RuntimeException(self::INVALID_PORT_MESSAGE);
        }
    
        return (int) $number;
    }
}
