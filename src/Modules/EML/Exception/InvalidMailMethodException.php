<?php
namespace CQM\Modules\EML\Exception;

class InvalidMailMethodException extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message, 500);
    }
}
