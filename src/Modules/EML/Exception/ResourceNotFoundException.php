<?php
namespace CQM\Modules\EML\Exception;

class ResourceNotFoundException extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message, 404);
    }
}