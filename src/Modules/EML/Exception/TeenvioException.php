<?php

namespace CQM\Modules\EML\Exception;

/**
 * @package API
 * @author Víctor J. Chamorro <victor@ipdea.com>
 * @copyright (c) Teenvio/Ipdea Land
 * @license LGPL v3
 */
class TeenvioException extends \Exception
{

}