<?php

namespace CQM\Modules\EML\Exception;

class InvalidCarrierException extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message, 500);
    }
}
