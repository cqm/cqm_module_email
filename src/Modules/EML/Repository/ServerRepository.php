<?php

namespace CQM\Modules\EML\Repository;

use CQM\Modules\EML\EML;
use CQM\Modules\EML\Entity\Application;
use CQM\Modules\EML\Entity\Server;
use CQM\Modules\EML\Util\UtilMessage;
use CQM\Modules\EML\Exception\ResourceNotFoundException;

/**
 * ServerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ServerRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Create a new CQM\Modules\EML\Entity\Server
     *
     * @param string $traveler  JSON formatted string
     * @return object $server CQM\Modules\EML\Entity\Server
     */
    public function createServer(&$traveler, $carrier, $ipAddress, $host, $port, $name, $charset, $description, $senderId, $senderMail, $senderName, $replyTo, $serverOrder = null, $serverData, $username, $password, $plan = null)
    {
        $em = $this->getEntityManager();

        $server = new Server();

        $server->setCarrier($carrier);
        $server->setIpAddress($ipAddress);
        $server->setHost($host);
        $server->setPort($port);
        $server->setServerName($name);
        $server->setCharset($charset);
        $server->setDescription($description);
        $server->setSenderId($senderId);
        $server->setSenderMail($senderMail);
        $server->setSenderName($senderName);
        $server->setReplyTo($replyTo);
        $server->setServerOrder($serverOrder);
        $server->setServerData($serverData);
        $server->setUsername($username);
        $server->setPassword($password);
        $server->setPlan($plan);

        $em->persist($server);
        $em->flush();

        $traveler = EML::travelerReturn($traveler, 1, UtilMessage::getMessage(UtilMessage::SERVER_CREATED, $server->getId()));

        return $server;
    }

    /**
     * Get one server by its identifier
     *
     * @param string $traveler  JSON formatted string
     * @return object $server CQM\Modules\EML\Entity\Server
     */
    public function getServer(&$traveler, $serverId)
    {
        $server = $this->find($serverId);

        if (is_null($server)) {
            $message = UtilMessage::getMessage(UtilMessage::SERVER_NOT_FOUND, $serverId);
            $traveler = EML::travelerReturn($traveler, -1, $message);
            throw new ResourceNotFoundException($message);
        }

        return $server;
    }

    /**
     * Get all Servers
     * 
     * @param string $traveler  JSON formatted string
     * @return array
     */
    public function getServers(&$traveler = '')
    {
        $servers = $this->findAll();
        
        if (empty($servers)) {
            $traveler = EML::travelerReturn($traveler, 1, UtilMessage::ZERO_RECORDS);
        }
        
        return $servers;
    }

    /**
     * Delete one CQM\Modules\EML\Entity\Server.
     * 
     * @param string $traveler  JSON formatted string
     * @param integer $id       ID of an existing CQM\Modules\EML\Entity\Server registry
     * 
     * @return string $traveler JSON formatted string
     */
    public function deleteServer(&$traveler, $serverId)
    {
        $em = $this->getEntityManager();
        
        $server = $this->find($serverId);
        
        if (!is_null($server)) {
            $em->remove($server);
            $em->flush();
        } else {
            $message = UtilMessage::getMessage(UtilMessage::RESOURCE_NOT_FOUND, $serverId);
            $traveler = EML::travelerReturn($traveler, -1, $message);
            throw new ResourceNotFoundException($message);
        }
        
        $traveler = EML::travelerReturn($traveler, 200, UtilMessage::getMessage(UtilMessage::SERVER_DELETED, $serverId));

        return true;
    }
}
