<?php

namespace CQM\Modules\EML\Repository;

use CQM\Modules\EML\EML;
use CQM\Modules\EML\Entity\Mail;
use CQM\Modules\EML\Util\UtilText;
use CQM\Modules\EML\Util\UtilMessage;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Tools\Pagination\Paginator;
use CQM\Modules\EML\Exception\ResourceNotFoundException;
use CQM\Modules\EML\Exception\InvalidMailMethodException;

/**
 * MailRepository
 *
 */
class MailRepository extends \Doctrine\ORM\EntityRepository
{   
    const SEND_MAIL_NOW_AND_SAVE_METHOD   = 'G';
    const SEND_MAIL_NOW_DONT_SAVE_METHOD  = 'E';
    const SEND_MAIL_LATER_AND_SAVE_METHOD = 'N';
    const MAIL_IS_BLACKLISTED = 2;
    
    public function renderContents(Mail $mail)
    {
        if (!is_null($mail->getTemplate())) {
            return $this->renderContentFromTemplate($mail);
        } else {
            return $this->renderContent($mail);
        }           
    }

    public function renderContent(Mail $mail)
    {
        // Replace mail contents with contact parameters
        $cleanContent = UtilText::replaceContentsFromParameters($mail->getSubject(), $mail->getBody(), $mail->getReplaceParameters());
        
        $mail->setSubject($cleanContent['subject']);
        $mail->setBody($cleanContent['body']);

        return $mail;
    }

    /**
     * Render the contents of the template assigned to the current mail
     */
    public function renderContentFromTemplate(Mail $mail, $replaceParameters = null)
    {
        $template = $mail->getTemplate();

        if (is_null($template)) {
            throw new ResourceNotFoundException(UtilMessage::getMessage(UtilMessage::MAIL_TEMPLATE_NOT_FOUND));
        }

        $templateContentCollection = $template->getContent();

        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("language", $mail->getLanguage()))
            ->setFirstResult(0)
            ->setMaxResults(1)
        ;

        $templateContents = $templateContentCollection->matching($criteria);

        if (!empty($templateContents)) {
            try {
                // Replace mail contents with the template ones
                $cleanContent = UtilText::replaceContentsFromParameters($templateContents[0]->getSubject(), $templateContents[0]->getBody(), $mail->getReplaceParameters());
                
                $mail->setSubject($cleanContent['subject']);
                $mail->setBody($cleanContent['body']);

            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        } else {
            throw new ResourceNotFoundException(UtilMessage::getMessage(UtilMessage::RESOURCE_NOT_FOUND));
        }

        return $mail;
    }

    /**
     * ! NOT IN USE
     * Replace the mail or template contents with syntax for Teenvio ("[[[ ]]]").
     * Variable values remain the same, Teenvio will handle the actual key:value pairs replacements.
     */
    public function renderContentForTeenvio(Mail $mail)
    {
        $template = $mail->getTemplate();

        if (is_null($template)) {

            $cleanContent = UtilText::replaceTemplateStringInterpolationCharacters($mail->getSubject(), $mail->getBody());
        
        } else {
            $templateContentCollection = $template->getContent();

            $criteria = Criteria::create()
                ->where(Criteria::expr()->eq("language", $mail->getLanguage()))
                ->setFirstResult(0)
                ->setMaxResults(1)
            ;

            $templateContents = $templateContentCollection->matching($criteria);

            if (!empty($templateContents)) {
                // Replace mail contents with the template ones
                $cleanContent = UtilText::replaceTemplateStringInterpolationCharacters($templateContents[0]->getSubject(), $templateContents[0]->getBody());
            } else {
                throw new ResourceNotFoundException(UtilMessage::getMessage(UtilMessage::RESOURCE_NOT_FOUND, $template->getId()));
            }
        }

        $mail->setSubject($cleanContent['subject']);
        $mail->setBody($cleanContent['body']);

        return $mail;
    }
    
    public function sendPendingMail($mail, $carrier, $sendBulk = false)
    {
        $mail = $this->handleMail($mail, $carrier, array('send' => true, 'save' => true, 'sendBulk' => $sendBulk));

        return $mail;
    }

    public function sendMailBasedOnMethod($mail, $carrier, $sendBulk = false)
    {
        switch ($mail->getMethod()) {
            case self::SEND_MAIL_LATER_AND_SAVE_METHOD: // N
                $mail = $this->handleMail($mail, $carrier, array('send' => false, 'save' => true, 'sendBulk' => $sendBulk));
            break;
            case self::SEND_MAIL_NOW_AND_SAVE_METHOD:   // G
                $mail = $this->handleMail($mail, $carrier, array('send' => true, 'save' => true, 'sendBulk' => $sendBulk));
            break;
            case self::SEND_MAIL_NOW_DONT_SAVE_METHOD:  // E
                $mail = $this->handleMail($mail, $carrier, array('send' => true, 'save' => false, 'sendBulk' => $sendBulk));
            break;
            default: 
                throw new InvalidMailMethodException(UtilMessage::getMessage(UtilMessage::MAIL_INVALID_METHOD));
            break;
        }

        return true;
    }

    public function handleMail($mail, $carrier, array $flags, $state = 0)
    {   
        $response = array();
        $response['state'] = $state;

        // Send mail
        if (isset($flags['send']) && $flags['send']) {
            try {
                
                if ($flags['sendBulk']) {
                    $response = $carrier->sendMailToGroup($mail);
                } else {
                    $response = $carrier->sendMail($mail);
                }

            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }
        
        // Save mail
        if (isset($flags['save']) && $flags['save']) {
            $parameters = $this->prepareResponseParametersToSaveMail($response);
            $this->save($mail, $parameters);
        }

        return $mail;
    }

    public function prepareResponseParametersToSaveMail($response)
    {
        $parameters['state'] = $response['state'];
        $parameters['date_sent'] = $response['date_sent'] ?? null;
        $parameters['control_data'] = $response['error_info'] ?? null;
        $parameters['send_id'] = $response['send_id'] ?? null;

        return $parameters;
    }

    public function save($mail, $parameters)
    {
        $em = $this->getEntityManager();

        $carrier = (!is_null($mail->getServer())) ? $mail->getServer()->getCarrier() : $mail->getApplication()->getTransactionalServer()->getCarrier();
        
        $mail->setState($parameters['state'])
             ->setDateSent($parameters['date_sent'])
             ->setControlData($parameters['control_data'])
             ->setCarrier($carrier)
             ->setSendId($parameters['send_id'])
        ;

        $em->persist($mail);
        $em->flush();

        return true;
    }

    public function persistMailBeforeSend($mail, $parameters)
    {
        $em = $this->getEntityManager();

        $carrier = (!is_null($mail->getServer())) ? $mail->getServer()->getCarrier() : $mail->getApplication()->getTransactionalServer()->getCarrier();

        $mail->setState($parameters['state'])
             ->setDateSent($parameters['date_sent'])
             ->setControlData($parameters['control_data'])
             ->setCarrier($carrier);

        $em->persist($mail);

        return true;
    }

    public function excludeMail(Mail $mail)
    {
        $controlData = UtilMessage::getMessage(UtilMessage::MAIL_IN_ROBINSON, $mail->getMailTo());

        if ($mail->getMethod() != self::SEND_MAIL_NOW_DONT_SAVE_METHOD) {
            $mail = $this->save($mail, array('state' => self::MAIL_IS_BLACKLISTED, 'control_data' => $controlData));
        } else {
            $mail->setControlData($controlData);
        }

        return $mail;
    }

    /**
     * TODO: DEPRECATE THIS METHOD, MOVE IT TO UtilMail::class
     */
    public function validateEmailAddress($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception(sprintf(UtilMessage::INVALID_MAIL_ADDRESS, $email), 400);
        }

        return true;
    }

    public function getEmailsSentToContact($applicationId, $emailAddress) : array
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('m.id, m.state, m.dateSent')
            ->from('\CQM\Modules\EML\Entity\Mail', 'm')
            ->where('m.mailTo = :email AND m.application = :application')
            ->orderBy('m.id', 'ASC')
            ->setParameters(array('email' => $emailAddress, 'application' => $applicationId))    
        ;

        $query = $qb->getQuery();
        $raw = $query->getArrayResult();
        $result = array();
        foreach ($raw as $key => $mail) {
            $result[$key]['id'] = $mail['id'];
            $result[$key]['state'] = array_search($mail['state'], EML::$mailStates);
            $result[$key]['date_sent'] = $mail['dateSent'] ?? null;
        }

        return $result;
    }

    public function getPaginatedResults($applicationId, array $filters, $currentPage = 1, $limit = 50)
    {
        $qb = $this->_em->createQueryBuilder()
                        ->select('m.id, m.mailTo, m.fromMail, m.state, m.subject, m.dateSent')
                        ->from('CQM\Modules\EML\Entity\Mail', 'm')
                        ->where('m.application = :application_id');
        
        list($andWhere, $parameters) = $this->prepareAndWhereClause($filters);
        if ('' !== $andWhere) {
            $qb->andWhere($andWhere);
        }
        $qb->setParameters(array_merge($parameters, ['application_id' => $applicationId]));

        $query     = $qb->getQuery();
        $paginator = $this->paginate($query, $currentPage, $limit);
        $maxPages  = (int) ceil($paginator->count() / $limit);
        
        return [
            'mails'    => $query->getArrayResult(),
            'maxPages' => $maxPages
        ];
    }

    private function paginate($dql, $page = 1, $limit = 50)
    {
        $paginator = new Paginator($dql);
        $paginator->setUseOutputWalkers(false); // wtf

        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) // Offset
            ->setMaxResults($limit); // Limit

        return $paginator;
    }

    private function prepareAndWhereClause(array $filters)
    {
        $andWhere = '';
        foreach ($filters as $key => $value) {
            if (null === $value) {
                unset($filters[$key]);
            } else {
                $andWhere = $this->andWhereConditionString($andWhere, $key);
            }
        }

        return [$andWhere, $filters];
    }

    private function andWhereConditionString(string $andWhere, string $key)
    {
        if (empty($andWhere)) {
            $andWhere .= "m.{$key} = :{$key}";
        } else {
            $andWhere .= " and m.{$key} = :{$key}";
        }

        return $andWhere;
    }
}
