<?php

namespace CQM\Modules\EML\Util\Carrier;

use CQM\Modules\EML\Entity\Mail;
use CQM\Modules\EML\Entity\Params;
use CQM\Modules\EML\Entity\Server;
use CQM\Modules\EML\Util\UtilDatabase;
use CQM\Modules\EML\Util\UtilMessage;
use CQM\Modules\EML\Util\Api\Teenvio\APISendEmail;
use CQM\Modules\EML\Util\Api\Teenvio\APIClientPOST;
//use CQM\Modules\EML\Util\Connection\HTTPConnection;

class TeenvioCarrier extends AbstractCarrier
{
    private $user;

    private $password;

    private $plan;

    private $api;

    public function __construct(Server $server)
    {
        parent::__construct($server);

        //$this->httpConnection = new HTTPConnection($server->getHost());
        
        // Prepare data for Teenvio api
        $this->user = $this->server->getUsername();
        $this->password = $this->server->getPassword();
        $this->plan = $this->server->getPlan();
        $this->url = $this->server->getHost();
        // Create the API object
        $this->api = new APIClientPOST($this->user, $this->plan, $this->password, $this->url);
    }

    /**
     * action: newsletter_save
     */
    public function saveNewsletter($name, $body)
    {
        $em = UtilDatabase::getEntityManager();

        try {
            $newsletterId = $this->api->saveNewsletter($name, $body);
            
            $param = new Params();
            $param->setParamKey('newsletter');
            $param->setParamValue($newsletterId);
            $em->persist($param);
            $em->flush();

        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return $newsletterId;
    }  

    public function getNewsletter($newsletterId)
    {
        // ... $this->api->getNewsletterData($newsletterId);
    }

    /**
     * action: group_save
     */
    public function saveGroup($name)
    {
        try {
            $groupId = $this->api->saveGroup($name);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return $groupId;
    }

    /**
     * action: contact_save
     */
    public function saveContact(array $contact)
    {
        try {
            $contactId = $this->api->saveContact($contact);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return $contactId;
    }

    /**
     * action: contact_group
     */
    public function addContactToGroups($email, $groupId)
    {
        try {
            $contactId = $this->api->groupContact($email, $groupId);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return true;
    }

    public function listContactsFromGroup($groupId)
    {
        try {
            $response = $this->api->getGroupContacts($groupId);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        return $response;
    }

    public function sendMail(Mail $mail)
    {
        $response = array(); 

        try {
            $senderId = $mail->getSenderId() ?? $this->server->getSenderId();
            // Save the contact
            $contact = $this->prepareContactArray($mail->getMailTo(), $mail->getMailToName());
            $contactId = $this->saveContact($contact);

            // Save newsletter
            $newsletterId = $this->saveNewsletter($mail->getCampaignName(), $mail->getBody());
            // Prepare mail contents
            $sendEmail = (new APISendEmail($this->api));

            $sendEmail->setSubject($mail->getSubject())
                ->setName($mail->getSubject())
                ->setSenderId($senderId) // Set sender id dynamically to change the sender
                ->setReplyTo($mail->getReplyTo())
                ->setIdNewsletter($newsletterId)
                ->setIdContact($contactId) // Send mail to one single contact
                ->setLangId($mail->getLanguage())
            ;
            
            // Send it
            $campaignId = $sendEmail->send();

            $response['state'] = 1;
            $response['date_sent'] = new \DateTime('now');
            $response['send_id'] = $campaignId;

        } catch (\Exception $e) {
            $response['state'] = -1;
            $response['error_info'] = $e->getMessage();
        }

        return $response;   
    }


    public function sendMailToGroup(Mail $mail)
    {
        $response = array();

        $groupId = $this->prepareGroupId($mail);
        
        try {
            // Save newsletter
            $newsletterId = $this->saveNewsletter($mail->getCampaignName(), $mail->getBody());
            // Prepare mail contents
            $sendEmail = (new APISendEmail($this->api))
                ->setSubject($mail->getSubject())
                ->setName($mail->getSubject())
                ->setSenderId($this->server->getSenderId())
                ->setReplyTo($mail->getReplyTo())
                ->setIdNewsletter($newsletterId)
                ->setIdGroup($groupId) // To send campaign to a full group/list
                ->setLangId($mail->getLanguage())
            ;

            // Get the contacts from the current Application
            // ! REFACTOR THIS
            // TODO: Get contacts and add them to the current group
            // $contacts = $mail->getApplication()->getContact(); 

            // foreach ($contacts as $contact) {
            //     // Save the contacts first
            //     $contactArray = $this->prepareContactArray($contact->getEmail(), $contact->getFullname());
            //     $this->saveContact($contactArray);

            //     $this->api->groupContact($contact->getEmail(), $groupId);
            // }
            // Send it
            $campaignId = $sendEmail->send();
            // Set response
            $response['state'] = 1;
            $response['date_sent'] = new \DateTime('now');
            $response['send_id'] = $campaignId;

        } catch (\Exception $e) {
            $response['state'] = -1;
            $response['error_info'] = $e->getMessage();
        }

        return $response;
    }
    

    public function prepareContactArray($email, $name)
    {
        return array('email' => $email, 'nombre' => $name);
    }

    public function prepareGroupId($mail)
    {
        if (is_null($mail->getGroupId())) {
            $groupId = $this->saveGroup('API New Group'); // save group name dynamically?
        } elseif ($this->api->getGroupData($mail->getGroupId())) { // check if the group exists
            $groupId = $mail->getGroupId();
        } else {
            throw new \Exception(UtilMessage::getMessage(UtilMessage::INVALID_GROUP_ERROR));
        }

        return $groupId;
    }

    /**
     * TODO: REFACTOR
     */
    public function getMailStats(Mail $mail)
    {
        $id = $mail->getSendId();
        
		if (empty($id)) {
			throw new \Exception(UtilMessage::getMessage(UtilMessage::INVALID_MAIL_SEND_ID));
		}
        
        $jsonResponse = $this->api->getStats($id, $this->api::OUTPUT_MODE_JSON);
        
        return $this->parseStats($mail, $jsonResponse);
    }
    
    /**
     * Parse the most obnoxious API response you could get from the folks @ Teenvio
     */
    private function parseStats(Mail $mail, $json)
    {
        $data = json_decode($json, true);
        $response = array();
        
        $response['id'] = $mail->getId();
        $response['recipient'] = array('email' => $mail->getMailTo());
        $response['subject'] = $data['subject'];
        $response['date_sent'] = new \DateTime($data['date']);
        $response['clicks'] = array(
            'opened' => $data['clicks_opend'],
            'unique' => $data['clicks_unique'],
            'all' => $data['clicks_all'],
        );
        $response['views'] = array(
            'opened' => $data['views_opend'],
            'unique' => $data['views_unique'],
            'all' => $data['views_all']
        );
        $response['contacts'] = array(
            'all' => $data['contacts']['all'],
            'sent' => $data['contacts']['send'],
            'unsent' => $data['contacts']['unsend']
        );
        
        return $response;
    }
}
