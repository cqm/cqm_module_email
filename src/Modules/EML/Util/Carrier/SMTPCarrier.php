<?php

namespace CQM\Modules\EML\Util\Carrier;

use PHPMailer\PHPMailer\PHPMailer;
use CQM\Modules\EML\Entity\Mail;
use CQM\Modules\EML\Entity\Server;
use CQM\Modules\EML\Util\UtilDatabase;
use CQM\Modules\EML\Util\UtilText;
use CQM\Modules\EML\Util\UtilFile;
use CQM\Modules\EML\Util\UtilMail;

class SMTPCarrier extends AbstractCarrier
{

    const CC = 'CC';
    const BCC = 'BCC';
    const CONTENT_TRANSFER_ENCODING_BASE64 = 'base64';
    const CONTENT_TRANSFER_ENCODING_QUOTED_PRINTABLE = 'quoted-printable';
    const CONTENT_TRANSFER_ENCODING_8BIT = '8bit';
    const CHARSET = 'UTF-8';

    /**
     * @var PHPMailer
     */
    private $smtp = null;
    private $limitedHosts = array(
        'api.teenvio.com',
        'api5.teenvio.com'
    );

    public function __construct(Server $server)
    {
        parent::__construct($server);
    }

    /**
     * Init PHPMailer
     */
    protected function initSMTP()
    {
        $this->smtp = new PHPMailer();

        $this->smtp->IsSMTP();

        $this->smtp->SMTPAuth = true;
        $this->smtp->Host = $this->server->getHost();
        $this->smtp->Port = $this->server->getPort();
        $this->smtp->Username = $this->server->getUsername();
        $this->smtp->Password = $this->server->getPassword();
        $this->smtp->CharSet = $this->server->getCharset();
        $this->smtp->Encoding = self::CONTENT_TRANSFER_ENCODING_QUOTED_PRINTABLE;
        $this->smtp->SMTPAutoTLS = true;
    }

    /**
     * Envía un email único
     * @param Mail $mail
     * @return array
     * @throws Exception
     */
    function sendMail(Mail $mail)
    {
        try {
            $em = UtilDatabase::getEntityManager();

            if (is_null($this->smtp)) {
                $this->initSMTP();
            }

            if (!is_null($mail->getReplyTo())) {
                $this->smtp->addReplyTo($mail->getReplyTo(), '');
            }

            $this->addMailFromAddressAndName($mail);

            $this->smtp->clearAllRecipients();
            $this->smtp->addAddress($mail->getMailTo());
            $this->smtp->isHTML(true);
            $this->smtp->Subject = $mail->getSubject();
            $this->smtp->Body = $mail->getBody();
            $this->smtp->AltBody = '';

            // Send "CC" and "BCC" if the current server allows it
            // also add attachments if there are any
            if ($this->checkCurrentHostAllowsMoreRecipients()) {
                $this->addExtraMailRecipients($mail);
                $this->addMailAttachments($mail);
            }

            $response['success'] = $this->smtp->send(); // send it

            if ($response['success']) {
                $response['state'] = 1;
                $response['date_sent'] = new \DateTime('now');

                UtilFile::clearMailAttachmentsFromDisk($mail);

            } else {
                $response['state'] = strpos($this->smtp->ErrorInfo, 'SMTP connect() failed') === 0 ? 0 : -1;
                $response['error_info'] = $this->smtp->ErrorInfo;
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return $response;
    }

    /**
     * This method checks if the current selected host can send emails to "CC" and "BCC"
     * i.e: Teenvio can't, so we need to discard it
     *
     * @return bool
     */
    private function checkCurrentHostAllowsMoreRecipients()
    {
        return !\in_array($this->smtp->Host, $this->limitedHosts);
    }

    /**
     * Add more recipients for CC and BCC
     * @param Mail $mail
     * @return void
     */
    private function addExtraMailRecipients(Mail $mail)
    {
        $this->addRecipients($mail->getBcc(), self::BCC);
        $this->addRecipients($mail->getCc(), self::CC);
    }

    private function addRecipients($recipients, $type)
    {
        $recipients = \json_decode($recipients, true);
        if (!empty($recipients)) {
            foreach ($recipients as $recipient) {
                if (!isset($recipient['email']) || !$this->addRecipient($recipient, $type)) {
                    continue;
                }
            }
        }
    }

    private function addRecipient($recipient, $type)
    {
        $name = $this->prepareRecipientFullName($recipient);

        if ($email = UtilMail::validateEmailAddress($recipient['email'])) {
            if ($type === self::BCC) {
                $this->smtp->addBCC($email, $name);
            } elseif ($type === self::CC) {
                $this->smtp->addCC($email, $name);
            }
        } else {
            return false;
        }

        return true;
    }

    /**
     * This method adds attachments to the PHPMailer object, if there are any
     *
     * @param Mail $mail
     * @return bool
     */
    private function addMailAttachments(Mail $mail)
    {
        $this->smtp->clearAttachments();

        $attachments = $mail->getAttachment();
        if (count($attachments)) {
            foreach ($attachments as $attachment) {

                // Save the file contents on disk to set a real path
                UtilFile::save($attachment);

                $this->smtp->addAttachment(
                    $attachment->getTempPath(),
                    $attachment->getOriginalFileName(),
                    PHPMailer::ENCODING_BASE64,
                    $attachment->getMimeType()
                );
            }
        }

        return true;
    }

    private function prepareRecipientFullName($recipient)
    {
        $name = '';
        if (isset($recipient['first_name'])) {
            $name .= $recipient['first_name'];
        }
        if (isset($recipient['last_name'])) {
            $name .= ' ' . $recipient['last_name'];
        }

        return $name;
    }

    private function addMailFromAddressAndName(Mail $mail)
    {
        $address = $mail->getFromMail();
        $name = $mail->getFromName() ?? $this->server->getSenderName();
        $this->smtp->setFrom($address, $name);
    }

}
