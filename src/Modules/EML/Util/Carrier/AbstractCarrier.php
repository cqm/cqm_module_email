<?php

namespace CQM\Modules\EML\Util\Carrier;

use CQM\Modules\EML\Entity\Server;
use CQM\Modules\EML\Util\UtilDatabase;
use CQM\Modules\EML\Entity\Mail;

abstract class AbstractCarrier
{
    protected $server = null;

    /**
     * Mail queue to send
     * @var array
     */
    protected $mQueue = [];

    protected $q_next = 0;

    protected $q_last = 0;

    protected $pid = -1;

    protected $ppid	= -1;

    protected $status = [];

    protected $error_log = '';

    public function __construct(Server $server)
    {   
        $this->server = $server;
    }
  
    /**
     * Add a new mail to the queue
     * @param Mail $mail
     * @return int 0 Ok, null err
     */
    function pushMail(Mail $mail) 
    {
        // Controlamos que, si el proceso está corriendo, ya solo puede acceder el propio proceso a estas operaciones
        if ($this->pid != -1 && $this->pid != getmypid()) {
        return false; // posible wtf? (legacy)
        }
        
        $this->mQueue[$this->q_last] = $mail;
        $this->q_last++;

        return true;
    }

    /**
     * Pop de el siguiente mail a enviar
     * @return NULL|Mail
     */
    protected function popMail()
    {
        // Controlamos que, si el proceso está corriendo, ya solo puede acceder el propio proceso a estas operaciones
        if ($this->pid != -1 && $this->pid != getmypid()) {
            return null; // posible wtf (legacy)
        }

        if ($this->q_next == $this->q_last) {
            return null;
        }

        // get the next message and remove it from the queue
        $mail = $this->mQueue[$this->q_next];
        unset($this->mQueue[$this->q_next]);

        $this->q_next++;

        return $mail;
    }

    /**
     * Send one mail
     * @param Mail $mail
     * @return int
     */
    abstract function sendMail(Mail $mail);

    function getMailStats(Mail $mail)
    {
        return [];
    }

    /**
     * Envía los emails multiples de la cola. Por defecto, realiza multiples llamadas a sendMail, pero se puede sobrescribir
     * para realizar métodos más eficientes
     */
    function sendMailMultiple()
    {
        while (!is_null($mail = $this->popMail())) {
            $response = $this->sendMail($mail);
            
            //$mail->save();
            $this->status[$mail->getId()] = $response;
        }
    }

    /**
     * Inicia un proceso que se dedica a enviar los emails de la cola, y, cuando la cola está vacía,
     * finaliza.
     */
    function startSending()
    {
        $this->ppid = getmypid();
        $pid = pcntl_fork(); // wtf

        if ($pid == -1) {
            return -1;
        } elseif ($pid) {
            $this->pid = $pid;
            return 0;
        } else {
            $ret = $this->sendMailMultiple();
            exit($ret);
        }
    }

    /**
     * Espera a que termine el proceso de envios.
     * @return int
     */
    function wait()
    {
        $res = pcntl_waitpid($this->pid, $status);

        return $this->status;
    }
}
