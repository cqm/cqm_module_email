<?php

namespace CQM\Modules\EML\Util\Api\Teenvio;

use CQM\Modules\EML\Exception\TeenvioException;
use CQM\Modules\EML\Util\Api\Teenvio\APIClientPOST;

class APISendEmail
{
	/**
	 * Instance APIClientPOST
	 * @var \Teenvio\APIClientPOST
	 */
	private $api = null;
	
	/**
	 * @var string
	 */
	private $subject = "";
	
	/**
	 * @var string
	 */
	private $name = "";
	
	/**
	 * @var int
	 */
	private $senderId = 0;

    /**
     * @var int|null
     */
    private $replyTo = null;
	
	/**
	 * @var int
	 */
	private $idGroup = 0;
	
	/**
	 * @var int
	 */
	private $idContact = 0;
	
	/**
	 * @var int
	 */
	private $idNewsletter = 0;
	
	/**
	 * @var string
	 */
	private $analytics = "";

    /**
     * @var string
     */
    private $langId = "";
	
	/**
	 * @var boolean
	 */
	private $header = true;
	
	/**
	 * @var boolean
	 */
	private $socialFooter = false;
	
	/**
	 * @var boolean
	 */
	private $shareHeader = false;
	
	/**
	 * @var string[string]
	 */
	private $vars = array();
		
	/**
	 * Object for Send emails under Teenvio API
	 * @param \Teenvio\APIClientPOST $apiInstance
	 * @throws TeenvioException
	 */
	public function __construct(APIClientPOST $apiInstance)
	{	
		if (!$apiInstance instanceof \CQM\Modules\EML\Util\Api\Teenvio\APIClientPOST) {
			throw new TeenvioException('$apiInstance is not instance of \CQM\Modules\EML\Util\Api\APIClientPOST');
		}

		$this->api = $apiInstance;
	}
	
	/**
	 * Email Subject
	 * @param string $subject
	 */
	public function setSubject($subject)
	{
		$this->subject = $subject;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getSubject()
	{
		return $this->subject;
	}

	/**
	 * Internal/private Name
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Teenvio Id of Sender
	 * @param int $senderId
	 */
	public function setSenderId($senderId) 
	{
		$this->senderId = (int) $senderId;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getSenderId()
	{
		return $this->senderId;
	}

    /**
     * Teenvio Id of Reply To
     * @param int $replyTo
     */
    public function setReplyTo($replyTo)
    {
        $this->replyTo = (int) $replyTo;

        return $this;
    }

    /**
     * @return int
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

	/**
	 * Teenvio Id of Group
	 * @param int $idGroup
	 */
	public function setIdGroup($idGroup) 
	{
		$this->idGroup = (int) $idGroup;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getIdGroup()
	{
		return $this->idGroup;
	}

	/**
	 * Teenvio Id of Contact
	 * @param int $idContact
	 */
	public function setIdContact($idContact) 
	{
		$this->idContact = (int) $idContact;

		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getIdContact()
	{
		return $this->idContact;
	}

	/**
	 * Teenvio Id of Newsletter
	 * @param int $idNewsletter
	 */
	public function setIdNewsletter($idNewsletter) 
	{
		$this->idNewsletter = (int) $idNewsletter;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getIdNewsletter()
	{
		return $this->idNewsletter;
	}

	/**
	 * Text for Google Analytics param
	 * @param string $analytics
	 */
	public function setAnalytics($analytics) 
	{
		$this->analytics = $analytics;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getAnalytics()
	{
		return $this->analytics;
	}

    /**
     * Text for Lang ID param
     * @param string $langId
     */
    public function setLangId($langId)
    {
        switch (strtoupper($langId)) {
            case 'ES':
                $this->langId = 3;
                break;
            case 'EN':
                $this->langId = 4;
                break;
            case 'PT':
                $this->langId = 11;
                break;
            case 'CA':
                $this->langId = 15;
                break;
            case 'EU':
                $this->langId = 18;
                break;
            default:
                $this->langId = 3;
                break;
        }
    }

    /**
     * @return string
     */
    public function getLangId()
    {
        return $this->langId;
    }

	/**
	 * Enable the email top bar: link of read into browser
	 * @param boolean $cab
	 */
	public function setHeader($header) 
	{
		$this->header = (bool) $header;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getHeader()
	{
		return $this->header;
	}

	/**
	 * Enable social footer: buttons of social networ profiles
	 * @param boolean $socialFooter
	 */
	public function setSocialFooter($socialFooter) 
	{
		$this->socialFooter = (bool) $socialFooter;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getSocialFooter()
	{
		return $this->socialFooter;
	}

	/**
	 * Enable extra top bar for share social buttons
	 * @param boolean $shareHeader
	 */
	public function setShareHeader($shareHeader) 
	{
		$this->shareHeader = (bool) $shareHeader;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getShareHeader()
	{
		return $this->shareHeader;
	}

	/**
	 * Add a custom var for the campaing
	 * @param string $varName
	 * @param string $varContent
	 */
	public function setVar($varName,$varContent)
	{
		$this->vars[$varName] = $varContent;

		return $this;
	}

	public function setVars(array $vars) 
	{
		$this->vars[] = $vars;

		return $this;
	}

	/**
	 * Send email, return the campaing id
	 * @return int
	 * @throws TeenvioException
	 */
	public function send()
	{
		if (empty($this->subject)) {
			throw new TeenvioException('Subject is empty');
		}
		
		if (empty($this->name)) {
			throw new TeenvioException('Name is empty');
		}
		
		if ($this->senderId == 0) {
			throw new TeenvioException('Sender is empty');
		}
		
		if ($this->idNewsletter == 0) {
			throw new TeenvioException('Newsletter content is empty');
		}
		
		if ($this->idContact == 0 && $this->idGroup == 0) {
			throw new TeenvioException('No recipients: select any group or any contact');
		}
		
		$response = 0;
		
		if ($this->idContact != 0) {
			$response = $this->api->sendEmailUnique($this->idContact, $this->idNewsletter, $this->senderId, $this->name, $this->subject, $this->analytics, $this->header, $this->shareHeader, $this->socialFooter, $this->langId, $this->vars, $this->replyTo);
		} else {
			$response = $this->api->sendEmail($this->idGroup, $this->idNewsletter, $this->senderId, $this->name, $this->subject, $this->analytics, $this->header, $this->shareHeader, $this->socialFooter, $this->langId, $this->vars, $this->replyTo);
		}
		
		return $response;
	}	
}
