<?php

namespace CQM\Modules\EML\Util\Connection;

class HTTPConnection
{
    private $url;

    private $method;

    private $https;

    private $certificates = [];

    public function __construct($url, $method = 'POST', $https = true)
    {
        $this->url = $url;
        $this->method = $method;
        $this->https = $https;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function isHttps()
    {
        return $this->https;
    }

    public function setHttps($https)
    {
        $this->https = $https;

        return $this;
    }

    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function addCertificate($certificate)
    {
        $this->certificates[] = $certificate;
    }

    public function makeRequest(array $parameters)
    {
        $queryParameters = http_build_query($parameters);

        $curl = $this->curlInit($queryParameters);

        return curl_exec($curl);
    }

    public function curlInit($queryParameters)
    {
        if ($this->getMethod() == 'POST') {
            $curl = curl_init($this->getUrl());
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $queryParameters);
        } elseif ($this->getMethod() == 'GET') {
            $curl = curl_init($this->getUrl().'?'.$queryParameters);
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl = $this->prepareSSL($curl);

        return $curl;
    }

    public function prepareSSL($curl)
    {
		if ($this->isHttps()) {
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
			foreach ($this->certificates as $certificate) {
				curl_setopt($curl, CURLOPT_CAINFO, getcwd().$certificate);
			}
        }
        
        return $curl;
    }
}
