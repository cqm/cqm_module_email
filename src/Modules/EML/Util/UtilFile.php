<?php

namespace CQM\Modules\EML\Util;

use CQM\Modules\EML\EML;
use CQM\Modules\EML\Entity\Attachment;
use CQM\Modules\EML\Entity\Mail;

class UtilFile
{
    public static function checkUploadedFile($filename)
    {
        return \is_uploaded_file($filename);
    }

    public static function checkUploadDir($uploadDir)
    {
        return \realpath($uploadDir);
    }

    public static function moveUploadedFile($filepath, $filename, $uploadDir)
    {
        $newFile = $uploadDir . $filename . time();

        if (!self::checkUploadedFile($filepath) 
            || !self::checkUploadDir($uploadDir)
            || !\move_uploaded_file($filepath, $newFile)) 
        {
            throw new \Exception('Error: File attachments cannot be processed!');
        }

        return $newFile;
    }

    public static function save(Attachment $attachment)
    {
        $filepath = EML::getTempDir() . time() . $attachment->getOriginalFileName();
        
        $fp = \fopen($filepath, 'w+');
        \fwrite($fp, \print_r($attachment->getData(), true));
        \fclose($fp);

        $attachment->setTempPath($filepath);

        return true;
    }

    public static function clearMailAttachmentsFromDisk(Mail $mail)
    {
        $attachments = $mail->getAttachment();

        if (\count($attachments)) {
            foreach ($attachments as $attachment) {
                $filepath = $attachment->getTempPath();
    
                if (!\realpath($filepath) || !\is_writable($filepath)) {
                    continue;
                }
        
                \unlink($filepath);
    
                $attachment->setTempPath("");
            }
        }
        
        return true;
    }
}