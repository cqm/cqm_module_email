<?php

namespace CQM\Modules\EML\Util;

class UtilMessage
{
    const APPLICATION_CREATED = 'Application with ID: %d created succesfully!';
    const APPLICATION_UPDATED = 'Application with ID: %d updated succesfully!';
    const APPLICATION_DELETED = 'Application with ID: %d deleted succesfully!';
    const SERVER_CREATED      = 'Server with ID: %d created succesfully!';
    const SERVER_UPDATED      = 'Server with ID: %d updated succesfully!';
    const SERVER_DELETED      = 'Server with ID: %d deleted succesfully!';
    const SERVER_NOT_FOUND    = 'Server with ID: %d not found!';
    const TEMPLATE_CREATED    = 'Template with ID: %d created succesfully!';
    const TEMPLATE_UPDATED    = 'Template with ID: %d updated succesfully!';
    const TEMPLATE_DELETED    = 'Template with ID: %d deleted succesfully!';
    const MAIL_CREATED        = 'Mail created succesfully!';
    const MAIL_UPDATED        = 'Mail with ID: %d updated succesfully!';
    const MAIL_CREATION_FAILED = 'Mail could not be created!';
    const MAIL_DELETED        = 'Mail with ID: %d deleted succesfully!';
    const RESOURCE_NOT_FOUND  = 'Resource with ID: %d not found!';
    const ZERO_RECORDS        = 'Zero records found!';
    const INVALID_RESOURCE    = 'Invalid resource!';
    const INVALID_MAIL_ADDRESS = 'ERROR: Invalid e-mail address: %s';
    const INVALID_MAIL_STATE = 'Invalid state: %s';
    const MAIL_STATE_NOT_FOUND = "State not found. Available states are: '%s', '%s', '%s' or '%s' ('%s' given).";
    const INVALID_MAIL_SEND_ID = 'Invalid or empty e-mail "send ID".';
    const INVALID_CARRIER = 'ERROR: Invalid carrier: %s. Valid carriers: %s';
    const MAIL_SEND_SUCCESS   = 'Mail succesfully sent!';
    const MAIL_SEND_FAILURE   = 'Mail could not be sent.';
    const MAIL_SAVE_FAILURE   = 'Mail could not be saved.';
    const MAIL_INVALID_METHOD = 'Invalid method to send emails!';
    const RECIPIENT_OK = 'Recipient %s is valid';
    const CONTACT_LIST_EMPTY  = 'The contact list must not be empty!';
    const MAIL_TEMPLATE_NOT_FOUND   = 'There is no template assigned to this mail!';
    const MAIL_QUEUE_EMPTY = 'E-mail queue is empty!';
    const MAIL_QUEUE_CREATED = 'E-mail queue created!';
    const MAIL_QUEUE_SEND_SUCCESS = 'Mails in queue sent succesfully! (%d failures)';
    const CONTACTS_ADDED   = 'Contacts added to application with ID: %d';
    const CONTACTS_REMOVED = 'Contacts removed from application with ID: %d';
    const CONTACT_NOT_FOUND = 'Contact not found!';
    const INVALID_GROUP_ERROR    = 'Error: Invalid group, mail could not be sent.';
    const MAIL_IN_ROBINSON_ERROR = 'Error: The email address: %s is in the Robinson list.';
    const MAIL_ADDED_TO_ROBINSON = 'The email address: %s was added to the Robinson list.';
    const MAIL_REMOVED_FROM_ROBINSON = 'The email address: %s was removed from the Robinson list.';
    const TEMPLATE_CONTENT_NOT_FOUND = "No contents found for the template with id: %d and language: '%s'";
    const MAIL_STATS_UNAVAILABLE = 'Stats unavailable for this e-mail.';
    
    public static function getMessage($message, $resource = 0)
    {
        return sprintf($message, $resource);
    }
}