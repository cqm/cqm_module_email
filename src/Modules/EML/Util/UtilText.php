<?php

namespace CQM\Modules\EML\Util;

class UtilText
{   
    /**
     * HTML to plain text
     * @param string $html
     * @return string $plaintext
     */
    public static function getPlainText(string $html){
        // remove comments and any content found in the the comment area (strip_tags only removes the actual tags).
        $plaintext = preg_replace('#<!--.*?-->#s', '', $html);
        // put a space between list items (strip_tags just removes the tags).
        $plaintext = preg_replace('#</li>#', ' </li>', $plaintext);
        // remove all script and style tags
        $plaintext = preg_replace('#<(script|style)\b[^>]*>(.*?)</(script|style)>#is', "", $plaintext);
        // remove br tags (missed by strip_tags)
        $plaintext = preg_replace("#<br[^>]*?>#", " ", $plaintext);
        // remove all remaining html
        $plaintext = strip_tags($plaintext);

        return $plaintext;
    }

    public static function replaceContentsFromParameters($subject, $body, $jsonParameters)
    {
        $parameters = json_decode($jsonParameters, true);

        if (!is_null($parameters) && !empty($parameters)) {
            foreach ($parameters as $key => $value) {
                $subject = preg_replace('/\{\{[ ]*'. $key .'[ ]*\}\}/', $value, $subject);
                $body = preg_replace('/\{\{[ ]*'. $key .'[ ]*\}\}/', $value, $body);
            }
        }

        return array('subject' => $subject, 'body' => $body);
    }

    /**
     * Replace "{{ }}" for "### ###", for Teenvio only
     */
    public static function replaceTemplateStringInterpolationCharacters($subject, $body) 
    {
        $pattern = array('/{{/','/}}/');
        $replacement = array('###', '###');

        $subject = preg_replace($pattern, $replacement, $subject);
        $body = preg_replace($pattern, $replacement, $body);
        
        return array('subject' => $subject, 'body' => $body);
    }
}
