<?php
namespace CQM\Modules\EML\Util;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class UtilDatabase
{
    protected static $em;
    
    protected static $instance = false;

    protected static $initialized = false;
    /**
     * Get an instance of the Database
     * @return Instance
	*/
    public static function getInstance($driver, $user, $pass, $host, $port, $database) 
    {
		if (!self::$instance) {
            $conn = self::prepareArrayForConnection($driver, $user, $pass, $host, $port, $database);
			self::$instance = new self($conn);
        }
        
		return self::$instance;
    }

    private function __construct($conn)
    {
        $paths = array(__DIR__."/../Resources/config/doctrine");
        $isDevMode = true;
        // Create a default YAML configuration yo manage the entities
        $config = Setup::createYAMLMetadataConfiguration($paths, $isDevMode);
        // Create Entity Manager    
        self::$em = EntityManager::create($conn, $config);

        self::$initialized = true;
    }
    
    public static function getEntityManager()
    {
        return self::$em;
    }

    public static function isInitialized()
    {
        return self::$initialized;
    }
    
    public static function prepareArrayForConnection($driver, $user, $pass, $host, $port, $database)
    {
        return array(
            'driver'   => $driver,
            'user'     => $user,
            'password' => $pass,
            'host'     => $host,
            'port'     => $port,
            'dbname'   => $database,
        );
    }
}
