<?php

namespace CQM\Modules\EML\Util;

class UtilMail
{
    public static function validateEmailAddress($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}