<?php

namespace CQM\Modules\EML;

use CQM\Modules\EML\Entity\Mail;
use CQM\Modules\EML\Entity\Server;
use CQM\Modules\EML\Entity\Content;
use CQM\Modules\EML\Entity\Contact;
use CQM\Modules\EML\Entity\Template;
use CQM\Modules\EML\Util\UtilMessage;
use CQM\Modules\EML\Util\UtilDatabase;
use CQM\Modules\EML\Entity\Application;
use CQM\Modules\EML\Util\Carrier\SMTPCarrier;
use CQM\Modules\EML\Util\Carrier\TeenvioCarrier;
use CQM\Modules\EML\Exception\ResourceNotFoundException;
use CQM\Modules\EML\Exception\InvalidCarrierException;
use CQM\Modules\EML\Util\UtilFile;
use CQM\Modules\EML\Entity\Attachment;

class EML
{
    const TRAVELER_OUTPUT_KEY = 'output';
    const TRAVELER_RETURN_CODE_KEY = 'return_code';
    const TRAVELER_USER_TEXT_KEY = 'user_text';
    const TRAVELER_LOG_TEXT_KEY = 'log_text';

    const LITERAL_SUCCESS = 'success';
    const USER_ERROR = 'user_error';
    const SERVER_ERROR = 'server_error';
    const EMPTY_VALUES = 'empty_values';
    const NOT_FOUND = 'not_found';
    const IN_ROBINSON = 'in_robinson';
    
    const CARRIER_NAME_SMTP = 'SMTP';
    const CARRIER_NAME_TEENVIO = 'Teenvio';

    const DEFAULT_METHOD = 'N';
    const DEFAULT_LANGUAGE = 'ES';
    const DEFAULT_CAMPAIGN_NAME = 'New Campaign';
    const DEFAULT_MAIL_STATE = 0;
    
    const STATE_PENDING_KEY = 'pending';
    const STATE_PENDING_VALUE = 0;
    const STATE_SENT_KEY = 'sent';
    const STATE_SENT_VALUE = 1;
    const STATE_BOUNCED_KEY = 'bounced';
    const STATE_BOUNCED_VALUE = 2;
    const STATE_FAILED_KEY = 'failed';
    const STATE_FAILED_VALUE = -1;

    const PAGINATION_LIMIT = 50;
    const PAGINATION_FIRST_PAGE = 1;

    public static $mailStates = array(
        self::STATE_PENDING_KEY => self::STATE_PENDING_VALUE,
        self::STATE_SENT_KEY => self::STATE_SENT_VALUE,
        self::STATE_BOUNCED_KEY => self::STATE_BOUNCED_VALUE,
        self::STATE_FAILED_KEY => self::STATE_FAILED_VALUE
    );

    private static $initialized = false; 

    private static $carriers = array(
        self::CARRIER_NAME_SMTP => SMTPCarrier::class,
        self::CARRIER_NAME_TEENVIO => TeenvioCarrier::class
    );

    private static $tempDir;

    public static function init($driver, $user, $pass, $host, $port, $database, $tempDir = null)
    {
        if (!self::$initialized) {
            UtilDatabase::getInstance($driver, $user, $pass, $host, $port, $database);

            if (!is_null($tempDir)) {
                self::setTempDir($tempDir);
            }
        } else {
            return self::class;
        }

        self::$initialized = true;
    }
    
    public static function setTempDir($tempDir)
    {
        self::$tempDir = $tempDir;
    }

    public static function getTempDir()
    {
        return self::$tempDir;
    }

    public static function prepareRecipient(&$traveler, $emailAddress, $firstName = '', $lastName = '', $parameters = array()) : array
    {        
        $em = UtilDatabase::getEntityManager();

        if ($em->getRepository(Mail::class)->validateEmailAddress($emailAddress)) {
            
            $traveler = self::travelerReturn($traveler, 200, UtilMessage::getMessage(UtilMessage::RECIPIENT_OK, $emailAddress));

            return array(
                'email' => $emailAddress,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'parameters' => $parameters
            );

        } else {
            throw new \Exception(UtilMessage::getMessage(UtilMessage::INVALID_MAIL_ADDRESS, $emailAddress));
        }
    }

    public static function prepareRecipients(&$traveler, $recipientsArray) : array
    {        
        $recipients = array();
        foreach ($recipientsArray as $recipient) {
            try {
                $recipients[] = self::prepareRecipient($traveler, $recipient['email'], $recipient['first_name'], $recipient['last_name'], $recipient['parameters']);
            } catch (\Exception $e) {
                continue; // jump to the next recipient if e-mail validation fails
            }
        }

        return $recipients;
    }

    /**
     * Add the recipient as a Contact and create one Mail object
     * @return \CQM\Modules\EML\Entity\Mail
     */
    public static function createMail(&$traveler, $applicationId, $recipientArray, $subject, $body, $language = 'ES', $method = 'N', $campaignName = 'New Campaign', $mailFrom = null, $serverId = null, $filesAttached = array(), $senderId = null, $cc = '', $bcc = '', $mailFromName = null, $replyTo = null)
    {
        $em = UtilDatabase::getEntityManager();
        
        try {
            // Get one server dynamically
            $server = (!is_null($serverId)) ? $em->getRepository(Server::class)->getServer($traveler, $serverId) : null;
            // Find an existing contact or add a new one
            $contact = $em->getRepository(Contact::class)->findOrAddContact($traveler, $recipientArray, $applicationId);
            // Create a Mail object
            $mail = (new Mail())
                ->setMailTo($contact->getEmail())
                ->setMailToName($contact->getFullName())
                ->setSubject($subject)
                ->setBody($body)
                ->setLanguage($language)
                ->setCampaignName($campaignName)
                ->setMethod($method)
                ->setState(0)
                ->setReplaceParameters($contact->getParameters())
                ->setServer($server)
                ->setCc($cc)
                ->setBcc($bcc)
                ->setReplyTo($replyTo)
            ;
            
            EML::addAttachments($mail, $filesAttached);

            if (!is_null($mailFrom) && $em->getRepository(Mail::class)->validateEmailAddress($mailFrom)) {
                $mail->setFromMail($mailFrom);
            }
            if (!is_null($mailFromName)) {
                $mail->setFromName($mailFromName);
            }

            // Set sender id for teenvio
            if (!is_null($senderId)) {
                $mail->setSenderId($senderId);
            }
            // Find the current application
            $application = $em->getRepository(Application::class)->getApplication($traveler, $applicationId);
            $mail->setApplication($application);

            $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);

        } catch (\Exception $ex) {
            $traveler = self::travelerReturn($traveler, -1, $ex->getMessage());
            throw $ex;
        }
        
        return $mail;
    }
    
    /**
     * @return \CQM\Modules\EML\Entity\Mail
     */
    public static function createMailFromTemplate(&$traveler, $templateId, $recipientArray, $language = 'ES', $method = 'N', $campaignName = 'New Campaign', $mailFrom = null, $serverId = null, $filesAttached = array(), $senderId = null, $cc = '', $bcc = '', $mailFromName = null, $replyTo = null)
    {
        $em = UtilDatabase::getEntityManager();

        try {
            // Get one server dynamically
            $server = (!is_null($serverId)) ? $em->getRepository(Server::class)->getServer($traveler, $serverId) : null;
            // Get the current template
            $template = $em->getRepository(Template::class)->getTemplate($traveler, $templateId);
            // Get application from template
            $application = $template->getApplication();
            // Find an existing contact or add a new one
            $contact = $em->getRepository(Contact::class)->findOrAddContact($traveler, $recipientArray, $application->getId());
        
            $mail = (new Mail())
                ->setTemplate($template)
                ->setMailTo($contact->getEmail())
                ->setMailToName($contact->getFullName())
                ->setLanguage($language)
                ->setCampaignName($campaignName)
                ->setMethod($method)
                ->setState(0)
                ->setReplaceParameters($contact->getParameters())
                ->setApplication($application)
                ->setServer($server)
                ->setCc($cc)
                ->setBcc($bcc)
                ->setReplyTo($replyTo)
            ;

            EML::addAttachments($mail, $filesAttached);
            // Set sender mail
            if (!is_null($mailFrom) && $em->getRepository(Mail::class)->validateEmailAddress($mailFrom)) {
                $mail->setFromMail($mailFrom);
            }
            if (!is_null($mailFromName)) {
                $mail->setFromName($mailFromName);
            }

            // Set sender id for teenvio
            if (!is_null($senderId)) {
                $mail->setSenderId($senderId);
            }

            $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);
        
        } catch(\Exception $ex) {
            $traveler = self::travelerReturn($traveler, -1, $ex->getMessage());
            throw $ex;
        }

        return $mail;
    }
    
    /**
     * Create a Mail object for each contact and returns an array of Mail::class objects
     * 
     * @param string $traveler      JSON formatted string
     * @param int $applicationId    Application::class object id
     * @param array $contacts       Array of Contact::class objects
     * @param string $language      Language code ('ES', 'EN')
     * @param string $campaignName  Campaign name for Teenvio
     * @param string $method        Send method
     * @param int $templateId       Template::class object id
     */
    public static function createMailQueue(&$traveler, $applicationId, $recipientsArray, $templateId = null, $subject = '', $body = '', $language = 'ES', $method = 'N', $campaignName = 'New Campaign', $mailFrom = null, $serverId = null, $filesAttached = array(), $senderId = null, $cc = '', $bcc = '', $mailFromName = null, $replyTo = null)
    {
        $em = UtilDatabase::getEntityManager();
        
        if (!is_null($templateId)) {
            // Get the current template
            $template = $em->getRepository(Template::class)->getTemplate($traveler, $templateId);
            // Get application from template
            $application = $template->getApplication();
        } else {
            $application = $em->getRepository(Application::class)->getApplication($traveler, $applicationId);
        }

        $mailQueue = array();

        if (!empty($recipientsArray)) {
            // Get one server dynamically
            $server = (!is_null($serverId)) ? $em->getRepository(Server::class)->getServer($traveler, $serverId) : null;
            $contacts = self::importContacts($traveler, $applicationId, $recipientsArray);

            foreach ($contacts as $contact) {
                
                $mail = (new Mail())
                    ->setMailTo($contact->getEmail())
                    ->setMailToName($contact->getFullName())
                    ->setSubject($subject)
                    ->setBody($body)
                    ->setLanguage($language)
                    ->setCampaignName($campaignName)
                    ->setMethod($method)
                    ->setState(0)
                    ->setReplaceParameters($contact->getParameters())
                    ->setApplication($application)
                    ->setServer($server)
                    ->setCc($cc)
                    ->setBcc($bcc)
                    ->setReplyTo($replyTo)
                ;
                
                EML::addAttachments($mail, $filesAttached);
                
                if (isset($template) && !is_null($template)) {
                    $mail->setTemplate($template);
                }
                // Set sender mail
                if (!is_null($mailFrom) && $em->getRepository(Mail::class)->validateEmailAddress($mailFrom)) {
                    $mail->setFromMail($mailFrom);
                }
                if (!is_null($mailFromName)) {
                    $mail->setFromName($mailFromName);
                }
                // Set sender id for teenvio
                if (!is_null($senderId)) {
                    $mail->setSenderId($senderId);
                }

                $mailQueue[] = $mail;
            }
    
            $traveler = self::travelerReturn($traveler, 200, UtilMessage::getMessage(UtilMessage::MAIL_QUEUE_CREATED));
        }

        return $mailQueue;
    }

    /**
     * ! DEPRECATED: Soon to be removed
     */
    public static function createBulkMail(&$traveler, $applicationId, $templateId = null, $subject = '', $body = '', $language = self::DEFAULT_LANGUAGE, $method = self::DEFAULT_METHOD, $campaignName = self::DEFAULT_CAMPAIGN_NAME, $mailFrom = null)
    {
        return false;
    }

    public static function getEmail(&$traveler, $mailId, $applicationId) 
    {
        $em = UtilDatabase::getEntityManager();
        
        if (is_null($applicationId)) {
            throw new \Exception('Application ID cannot be null');
        }
            
        $mail = $em->getRepository(Mail::class)->findOneBy(array('id' => $mailId, 'application' => $applicationId));

        if (is_null($mail)) {
            $message = UtilMessage::getMessage(UtilMessage::RESOURCE_NOT_FOUND, $mailId);
            $traveler = self::travelerReturn($traveler, -1, self::EMPTY_VALUES, self::EMPTY_VALUES);
            throw new ResourceNotFoundException($message);
        }

        $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);

        return $mail;
    }
    
    public static function getAllMails(&$traveler, $applicationId)
    {
        $em = UtilDatabase::getEntityManager();
        $mails = $em->getRepository(Mail::class)->findBy(array('application' => $applicationId));
        $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);

        return $mails;
    }

    public static function getEmails(&$traveler, $findBy)
    {
        $em = UtilDatabase::getEntityManager();

        if (!isset($findBy['state'])) {
			unset($findBy['state']);
        }
        
        $mails = $em->getRepository(Mail::class)->findBy($findBy);
        $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);

        return $mails;
    }

    public static function getPaginatedEmails(&$traveler, $applicationId, array $filters, $currentPage = 1, $limit = 50)
    {
        $em       = UtilDatabase::getEntityManager();
        $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);

        return $em->getRepository(Mail::class)->getPaginatedResults($applicationId, $filters, $currentPage, $limit);
    }

    /**
     * ! Stats are only available for carriers that send e-mails through API. Stats for SMTP are not available, for now 
     * @return array
     */
    public static function getMailStats(&$traveler, $mailId, $applicationId)
    {
		// Get current mail
		$mail = self::getEmail($traveler, $mailId, $applicationId);
        // Get the server dynamically assigned to the Mail or the Application default one
        $server = $mail->getServer() ?? $mail->getApplication()->getTransactionalServer();
        // Get the carrier assigned to the server
        $carrierName = $server->getCarrier();
        // Throw exception if the carrier used to send this email is SMTP or it does not have a sendId stored
        if (is_null($mail->getSendId())
            || $carrierName == self::CARRIER_NAME_SMTP 
                || $mail->getCarrier() == self::CARRIER_NAME_SMTP
        ) {
            throw new \Exception(UtilMessage::MAIL_STATS_UNAVAILABLE);
        }
        // Get the carrier instance
        $carrier = new self::$carriers[$carrierName]($server);

        try {
            $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);
            // Return array with mail stats
            return $carrier->getMailStats($mail);

        } catch (\Exception $ex) {
            throw new \Exception(UtilMessage::MAIL_STATS_UNAVAILABLE);
        }
    }

    /**
	 * Envío de un email único
	 * @param string $traveler
	 * @param Mail $mail
     * ! DEPRECATED PARAM @param $useTemplate  Flag to render mail contents from a template
	 */
    public static function send(&$traveler, Mail $mail, $useTemplate = false)
    {
        // Get Entity Manager
        $em = UtilDatabase::getEntityManager();
        // Get Mail repository
        $mailRepository = $em->getRepository(Mail::class);
        // Get the current Application
        $application = $mail->getApplication();
        // Get the server dynamically assigned to the Mail or the Application default one
        $server = $mail->getServer() ?? $application->getTransactionalServer();
        // Set sender email if its not set
        if (is_null($mail->getFromMail())) {
            $mail->setFromMail($server->getSenderMail());
        }

        $mail = $mailRepository->renderContents($mail);    
        // Check if the email address is blacklisted
        // if ($application->checkRobinsonList($mail->getMailTo()) || !$application->checkWhitelist($mail->getMailTo())) {
        if ($application->checkRobinsonList($mail->getMailTo())) {
            // Then don't send this mail
            $traveler = self::travelerReturn($traveler, 0, UtilMessage::getMessage(UtilMessage::MAIL_IN_ROBINSON, $mail->getMailTo()));
            return $mailRepository->excludeMail($mail);
        }

        // Get the carrier assigned to the server
        $carrierName = $server->getCarrier();
        // Get an instance of the carrier
        $carrier = new self::$carriers[$carrierName]($server);

        try {
            // Depending on the method selected, the mail will be handled differently
            $mailRepository->sendMailBasedOnMethod($mail, $carrier);

        } catch (\Exception $e) {
            $traveler = self::travelerReturn($traveler, -1, self::SERVER_ERROR, $e->getMessage());
            echo $e->getMessage();
            return $mail;
        }
        
        if ($mail->getState() < 0) {
            $traveler = self::travelerReturn($traveler, -1, self::SERVER_ERROR, self::SERVER_ERROR);
        } else {
            $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);
        }
        
        return true;	
    }

    /**
     * Store each e-mail in the queue
     */
    public static function sendMailQueue(&$traveler, $mailQueue)
    {
        $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);
        // Get entity manager
        $em = UtilDatabase::getEntityManager();
        // Get Mail repository
        $mailRepository = $em->getRepository(Mail::class);
        // Get the current Application
        $application = $mailQueue[0]->getApplication();
        // Get the server dynamically assigned to the Mail or the Application default one
        $server = $mailQueue[0]->getServer() ?? $application->getTransactionalServer();
        // Loop thru each email and send it
        if (count($mailQueue) > 0) {
            foreach ($mailQueue as $mail) {
                // Set sender mail if its not set
                if (is_null($mail->getFromMail())) {
                    $mail->setFromMail($server->getSenderMail());
                }
                // Render mail contents
                $mail = $mailRepository->renderContents($mail);
                 
                // Check if the email address is blacklisted, don't send this mail
                if ($application->checkRobinsonList($mail->getMailTo())) {
                    $traveler = self::travelerReturn($traveler, 0, UtilMessage::getMessage(UtilMessage::MAIL_IN_ROBINSON, $mail->getMailTo()));
                    $mailRepository->excludeMail($mail);
                    continue;
                }
                
                try {
                    // Store each mail directly in database
                    $parametersToSave = $mailRepository->prepareResponseParametersToSaveMail(array('state' => self::DEFAULT_MAIL_STATE));
                    $mailRepository->persistMailBeforeSend($mail, $parametersToSave);
                    $em->flush();
                } catch (\Exception $e) {
                    $traveler = self::travelerReturn($traveler, -1, UtilMessage::MAIL_SEND_FAILURE);
                    continue;
                }
            }
        } else {
            $traveler = self::travelerReturn($traveler, -1, UtilMessage::MAIL_QUEUE_EMPTY, self::SERVER_ERROR);
        }

        return true;
    }

    /**
     * ! DEPRECATED: Soon to be removed
     * Send a mail to a group of recipients (for Teenvio POST-API only)
     */
    public function sendBulk(&$traveler, Mail $mail, array $recipients, $groupId = null)
    {
        return false;
        // Get entity manager
        $em = UtilDatabase::getEntityManager();
        // Get Mail repository
        $mailRepository = $em->getRepository(Mail::class);
        // Get the server assigned to the Application
        $server = $mail->getApplication()->getBulkServer();
        // Check Teenvio is the selected carrier
        if ($server->getCarrier() !== self::CARRIER_NAME_TEENVIO) {
            throw new InvalidCarrierException(sprintf(UtilMessage::INVALID_CARRIER, $server->getCarrier(), self::CARRIER_NAME_TEENVIO));
        }
        // Set sender mail if its not set
        if (is_null($mail->getFromMail())) {
            $mail->setFromMail($server->getSenderMail());
        }
        // Add an existing group to the mail
        if (!is_null($groupId)) {
            $mail->setGroupId($groupId);
        }
        // Get an instance of the carrier assigned to the server (Teenvio only for now)
        $carrier = new self::$carriers[$server->getCarrier()]($server);

        try {
            // Send mail depending on the specified method ("G", "N" or "E")
            $mailRepository->sendMailBasedOnMethod($mail, $carrier, $sendBulk = true);

        } catch (\Exception $e) {
            $traveler = self::travelerReturn($traveler, -1, self::SERVER_ERROR, $e->getMessage());
            return $mail;
        }

        if ($mail->getState() < 0) {
            $traveler = self::travelerReturn($traveler, -1, self::SERVER_ERROR, self::SERVER_ERROR);
        } else {
            $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);
        }
    
        return $mail;	    
    }

    /**
     * Send all the pending emails (with state = 0) from one application.
     * This method will be called from a CLI command or cronjob
     */
    public static function sendPendingEmails(&$traveler, $state = '0', $limit = 50)
    {
        // Get Entity Manager
        $em = UtilDatabase::getEntityManager();
        // Get the current connection
        $conn = $em->getConnection();
        // START TRANSACTION
        $conn->beginTransaction();
        // Set autocommit to 0
        $conn->setAutoCommit(false);
        // Build the DQL query
        $query = $em->createQuery('SELECT m FROM CQM\Modules\EML\Entity\Mail m WHERE m.state = :state');
        $query->setParameter("state", $state);
        // Set limit of emails
        $query->setMaxResults($limit);
        // SELECT ... FOR UPDATE
        // $query->setLockMode(\Doctrine\DBAL\LockMode::PESSIMISTIC_WRITE);
        // Get array of Mail objects
        $mails = $query->getResult();

        $totalMails = count($mails);

        if ($totalMails <= 0) {
            return $totalMails;
        }
        
        // Send each mail
        foreach ($mails as $mail) {
            try {
                self::sendPending($traveler, $mail);
            } catch (\Exception $e) {
                continue;
            }
            // Commit connection
            $conn->commit();
        }
        // CLOSE CONNECTION
        $conn->close();

        $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS);

        return $totalMails;
    }

    /**
     * Send a mail with state = 0
     * This method will be called from the sendPendingEmails() method right above this one
     */
    public static function sendPending(&$traveler, Mail $mail)
    {
        // Get Entity Manager
        $em = UtilDatabase::getEntityManager();
        // Get Mail repository
        $mailRepository = $em->getRepository(Mail::class);
        // Get the current Application
        $application = $mail->getApplication();
        // Get the server dynamically assigned to the Mail or the Application default one
        $server = $mail->getServer() ?? $application->getTransactionalServer();
        // Check if the email address is blacklisted
        if ($application->checkRobinsonList($mail->getMailTo())) {
            // Then don't send this mail
            $mailRepository->excludeMail($mail);
            $traveler = self::travelerReturn($traveler, -1, self::IN_ROBINSON, self::IN_ROBINSON);
            return $mail;
        }
        // Get the carrier assigned to the server
        $carrierName = $server->getCarrier();
        // Get an instance of the carrier
        $carrier = new self::$carriers[$carrierName]($server);

        try {
            $mailRepository->sendPendingMail($mail, $carrier);
        } catch (\Exception $e) {
            $traveler = self::travelerReturn($traveler, -1, self::SERVER_ERROR, $e->getMessage());
            throw new \Exception($e->getMessage());
        }
        
        return true;	
    }

    /**
	 * Create a Template and return its id
	 * 
	 * @param string $traveler
	 * @param int $applicationId
	 * @param string $language
	 * @param string $subject
	 * @param string $body
	 * @return int|null
	 */
    public static function createTemplate(&$traveler, $applicationId, $subject, $body, $language = 'ES')
    {
        $em = UtilDatabase::getEntityManager();
        // Find the current application
        $application = $em->getRepository(Application::class)->getApplication($traveler, $applicationId);

        $template = new Template();
        
        try {
            $content = $em->getRepository(Content::class)->prepareContent($template, $language, $subject, $body);
            
            $template->addContent($content);
            $template->setApplication($application);
            $em->persist($template);
            $em->flush();

        } catch (\Exception $e) {
            $traveler = self::travelerReturn($traveler, -1, self::SERVER_ERROR, $e->getMessage());
            throw new \Exception($e->getMessage());
        }

        $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);

        return $template->getId();
    }

    /**
	 * Update a Template and return its id
	 * 
	 * @param string $traveler
	 * @param int $templateId
	 * @param string $language
	 * @param string $subject
	 * @param string $body
	 * @return int|null
	 */
    public static function updateTemplate(&$traveler, $applicationId, $templateId, $subject, $body, $language = 'ES') 
    {
        $em = UtilDatabase::getEntityManager();
        
        try {
            $template = $em->getRepository(Template::class)->getTemplate($traveler, $templateId, $applicationId);

            $content = $em->getRepository(Content::class)->updateContent($template, $language, $subject, $body);
            
            $template->addContent($content);
            $em->merge($template);
            $em->flush();

        } catch (\Exception $e) {
            $traveler = self::travelerReturn($traveler, -1, self::SERVER_ERROR, $e->getMessage());
            throw new \Exception($e->getMessage());
        }

        $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);

        return $template->getId();
    }

    public function getCarriers(&$traveler)
    {
        $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);
        return array_keys(self::$carriers);
    }

    /**
     * Función para sustituir en un futuro por accesos a diccionario
     * 
     * @param string $lang Idioma deseado
     * @param string $text Texto
     * @param string ...$args Argumentos para sustituir en los textos
     * 
     * @return string
     */
    public static function dictionary($language, $text, ...$args)
    {
        // Replace the args
        for ($i = 0; $i < count($args); $i++) {
            $text = str_replace('$'.$i, $args[$i], $text);
        }
        
        return $text;
    }

    /**
     * Función que simplifica el acceso al traveller para actualizar los valores de retorno del mismo
     *
     * @param string $traveler      Traveler en formato json según llega
     * @param int $returnCode    Código de retorno
     * @param string $logText       Texto para el Log
     * @param string $userText      Texto para el usuario
     * 
     * @return string $traveler     JSON formatted string
     */
    public static function travelerReturn($traveler, $returnCode = 1, $userText = '', $logText = '')
    {
        $traveler = json_decode($traveler, true);
        
        $traveler[self::TRAVELER_OUTPUT_KEY][self::TRAVELER_RETURN_CODE_KEY] = $returnCode;
        $traveler[self::TRAVELER_OUTPUT_KEY][self::TRAVELER_USER_TEXT_KEY] = $userText;
        $traveler[self::TRAVELER_OUTPUT_KEY][self::TRAVELER_LOG_TEXT_KEY] = $logText;
        
        return json_encode($traveler);
    }

    /**
     * Add contacts to an application.
     * 
     * @param string $traveler          JSON formatted string
     * @param integer $applicationId    Application ID
     * @param array $contactList        Array of contacts (email, firstName, lastName)
     * 
     * @return array array of Contact objects
     */
    public static function importContacts(&$traveler, $applicationId, array $contactList = [])
    {
        $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);
        $em = UtilDatabase::getEntityManager();

        if (!empty($contactList)) {
            $contactContainer = array();
            foreach ($contactList as $contactData) {
                $contactContainer[] = $em->getRepository(Contact::class)->findOrAddContact($traveler, $contactData, $applicationId);
            }

        } else {
            $traveler = self::travelerReturn($traveler, -1, self::EMPTY_VALUES, self::EMPTY_VALUES);
        }


        return $contactContainer;
    }

    /**
     * Get all contacts from one application
     */
    public static function getContacts(&$traveler, $applicationId)
    {
        $em = UtilDatabase::getEntityManager();
        $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);

        $contacts = $em->getRepository(Contact::class)->findBy(array('application' => $applicationId));
        $response = array();
        foreach ($contacts as $key => $contact) {
            $response[$key]['email'] = $contact->getEmail();
            $response[$key]['first_name'] = $contact->getFirstName();
            $response[$key]['last_name'] = $contact->getLastName();
            $response[$key]['parameters'] = json_decode($contact->getParameters(), true);
        }
        
        return $response;   
    }

    /**
     * Return the state of the email. If getKey is true, it will return the state key (ie: "pending", "sent", ...)
     * otherwise it will return the numeric value of the state
     */
    public static function getMailState($state, $getKey = false)
    {
        if (null === $state || !isset(self::$mailStates[$state]) && !$getKey) {
            return null;
        }

        if ($getKey) {
            return array_search($state, self::$mailStates);
        }

        return self::$mailStates[$state];
    }

    public static function getContactEmails(&$traveler, $applicationId, $emailAddress)
    {
        $em = UtilDatabase::getEntityManager();
        $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);
        
        return $em->getRepository(Mail::class)->getEmailsSentToContact($applicationId, $emailAddress);
    }

    public static function deleteContact(&$traveler, $applicationId, $emailAddress)
    {
        $em = UtilDatabase::getEntityManager();
        $traveler = self::travelerReturn($traveler, 1, self::LITERAL_SUCCESS, self::LITERAL_SUCCESS);

        try {
            $em->getRepository(Contact::class)->removeContact($traveler, $applicationId, $emailAddress);
        } catch (\Exception $ex) {
            throw $ex;
        }

        return true;
    }

    /**
     * Add file attachments from CQM Email API (check API for reference about $files array)
     * @param Mail $mail
     * @param array $files
     */
    public static function addAttachments(Mail $mail, $files)
    {
        if (!empty($files)) {
            foreach ($files as $file) {
                // New Attachment object
                $attachment = (new Attachment())
                    ->setData(\file_get_contents($file['realPath']))
                    ->setOriginalFileName($file['originalName'])
                    ->setOriginalFileSize($file['size'])
                    ->setMimeType($file['mimeType'])
                    ->setFileExtension($file['fileExtension'])
                ;
                
                $attachment->setMail($mail);
                $mail->addAttachment($attachment);
            }
        }

        return true;
    }

    public static function createMailToForward(&$traveler, $applicationId, $emailId)
    {
        $em = UtilDatabase::getEntityManager();
        $mailToForward = self::getEmail($traveler, $emailId, $applicationId);
        $serverId = $mailToForward->getServer() ? $mailToForward->getServer()->getId() : $mailToForward->getApplication()->getTransactionalServer()->getId();    
        $contact = $em->getRepository(Contact::class)->findOneByEmail($mailToForward->getMailTo());
        $recipient = self::prepareRecipient($traveler, $contact->getEmail(), $contact->getFirstName(), $contact->getLastName(), json_decode($contact->getParameters(), true));
        $files = self::recoverFilesFromMailAttachments($mailToForward);
        
        return self::createMail(
            $traveler, 
            $applicationId, 
            $recipient, 
            $mailToForward->getSubject(), 
            $mailToForward->getBody(), 
            $mailToForward->getLanguage(), 
            self::DEFAULT_METHOD, 
            $mailToForward->getCampaignName(), 
            $mailToForward->getFromMail(), 
            $serverId, 
            $files, 
            $mailToForward->getSenderId(), 
            $mailToForward->getCc(), 
            $mailToForward->getBcc()
        );
    }

    private static function recoverFilesFromMailAttachments(Mail $mail)
    {
        $attachments = $mail->getAttachment();

        $files = [];
        foreach ($attachments as $key => $attachment) {

            $filepath = sys_get_temp_dir() . time() . $attachment->getOriginalFileName();
            $fp = \fopen($filepath, 'w+');
            \fwrite($fp, \print_r($attachment->getData(), true));
            \fclose($fp);

            $files[$key]['realPath'] = $filepath;
            $files[$key]['originalName'] = $attachment->getOriginalFileName();
            $files[$key]['size'] = $attachment->getOriginalFileSize();
            $files[$key]['mimeType'] = $attachment->getMimeType();
            $files[$key]['fileExtension'] = $attachment->getFileExtension();
        }

        return $files;
    }
}
