<?php

namespace CQM\Modules\EML\Composer;

use CQM\Modules\EML\EML;
use CQM\Modules\EML\Util\UtilDatabase;
use Composer\Script\Event;

class ModuleInstaller
{   
    public static function install(Event $event)
    {
        // Get Composer\IO\IOInterface instance
        $io = $event->getIO();

        $io->write([
            PHP_EOL,
            '<comment>CQM Email Module Installation</>',
            '<comment>=============================</>',
            PHP_EOL
        ], true);
        
        $io->write([
            'To correctly install the <info>CQM Email Module</>, it is necessary to update your database schema.',
            'To do so, some connection parameters are needed (database host, user, password...)',
        ], true);

        $io->write(PHP_EOL, true);

        $proceed = $io->askConfirmation('Do you want to proceed with the installation? (yes/no) [<comment>yes</>] ');

        $io->write(PHP_EOL, true);

        if (!$proceed) {
            $io->warning(' [WARNING] CQM Module installation incomplete! Re-run the command: `composer install` to complete the installation.');
            return;
        }

        $io->write(['Please, provide the following connection parameters: (Press <comment><Enter></> to use the default values)'], PHP_EOL, true);
            $io->write(PHP_EOL, true);
        $params['db_driver'] = $io->ask('> Database driver [<comment>pdo_mysql</>]: ', 'pdo_mysql');
            $io->write(PHP_EOL, true);
        $params['db_host'] = $io->ask('> Database host [<comment>127.0.0.1</>]: ', '127.0.0.1');
            $io->write(PHP_EOL, true);
        $params['db_port'] = $io->askAndValidate('> Database port [<comment>3306</>]: ', function($number) {
            return self::validateInputNumber($number);
        }, null, '3306');
            $io->write(PHP_EOL, true);
        $params['db_user'] = $io->ask('> User [<comment>root</>]: ', 'root');
            $io->write(PHP_EOL, true);
        $params['db_pass'] = $io->askAndHideAnswer('> Password: ');
            $io->write(PHP_EOL, true);
        $params['db_name'] = $io->askAndValidate('> Database name: ', function($string) {
            return self::validateStringNotEmpty($string);
        });
        
        $continue = $io->askConfirmation('Ok, your database schema will get updated. Continue? (yes/no) [<comment>yes</>] ');

        $io->write(PHP_EOL, true);

        if (!$continue) {
            $io->warning(' [WARNING] CQM Module installation incomplete! Re-run the command: `composer install` to complete the installation.');
            return;
        }
        
        $io->write(PHP_EOL, true);

        // Send params to script handler and update the schema
        $response = ScriptHandler::updateSchema($params);

        $io->write(PHP_EOL, true);

        if ($response) {
            $io->write('✅ CQM Email Module installed succesfully!');
        } else {
            $io->error('! [ERROR] Installation failed. Some of the parameters are incorrect. Please, re-run the command: `composer install` to complete the installation.');
            return;
        }

    }

    public static function validateStringNotEmpty($string)
    {
        if ($string == '') {
            throw new \RuntimeException('You must provide a valid string.');
        }

        return $string;
    }

    public static function validateInputNumber($number)
    {
        if (!is_numeric($number)) {
            throw new \RuntimeException('You must type a valid port number.');
        }
    
        return (int) $number;
    }
}