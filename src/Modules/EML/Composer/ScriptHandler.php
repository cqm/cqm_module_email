<?php

namespace CQM\Modules\EML\Composer;

use CQM\Modules\EML\EML;
use CQM\Modules\EML\Util\UtilDatabase;

class ScriptHandler
{   
    /**
     * Update the schema using Doctrine Schema Tool.
     */
    public static function updateSchema(array $params)
    {
        try {
            $db = UtilDatabase::getInstance($params['db_driver'], $params['db_user'], $params['db_pass'], $params['db_host'], $params['db_port'], $params['db_name']);
            $em = $db::getEntityManager();

            $tool = new \Doctrine\ORM\Tools\SchemaTool($em);

            $classes = array(
                $em->getClassMetadata(\CQM\Modules\EML\Entity\Application::class),
                $em->getClassMetadata(\CQM\Modules\EML\Entity\Server::class),
                $em->getClassMetadata(\CQM\Modules\EML\Entity\Contact::class),
                $em->getClassMetadata(\CQM\Modules\EML\Entity\Content::class),
                $em->getClassMetadata(\CQM\Modules\EML\Entity\Mail::class),
                $em->getClassMetadata(\CQM\Modules\EML\Entity\Params::class),
                $em->getClassMetadata(\CQM\Modules\EML\Entity\Robinson::class),
                $em->getClassMetadata(\CQM\Modules\EML\Entity\Template::class),
                $em->getClassMetadata(\CQM\Modules\EML\Entity\Whitelist::class),
                $em->getClassMetadata(\CQM\Modules\EML\Entity\Nonce::class),
                $em->getClassMetadata(\CQM\Modules\EML\Entity\Attachment::class)
            );
        
            $tool->updateSchema($classes, true);
            
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}